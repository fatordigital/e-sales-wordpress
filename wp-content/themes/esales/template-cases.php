<?php
/*
 * Template Name: Cases
 */
?>
<?php get_header(); ?>

<main>
	<?php
	while(have_posts()) {
		the_post();

		$img = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' );

		?>
		<div class="hero">
			<div class="overlay">
				<div class="container valign">
					<div class="center">
						<h2>Cases de sucesso</h2>
					</div><i></i>
				</div>
			</div>

			<div class="bg" style="background-image: url(<?php echo $img[0]; ?>);"></div>
		</div>
		<?php

	}
	?>
	<div class="main-content">
		<div class="bg-esq"></div>
		<div class="bg-dir"></div>
		<div class="bg-center">
			<div class="bg-center-inner">
				<svg class="img" viewBox="0 0 1920 657" style="background-color:#ffffff00" version="1.1"
					xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xml:space="preserve"
					x="0px" y="0px" width="1920px" height="657px">
					<path class="shape-bg" d="M 0 0 L 523 57 L 960 2.7641 L 1397 57 L 1920 3.191 L 1920 657 L 0 657 L 0 0 Z" fill="#ffffff"/>
				</svg>
			</div>
		</div>

		<div class="sec-lista-cases">
			<div class="container">
				<div class="row">
					<div class="col-lg-8 center-block aligncenter">
						<h4>Conhecer nossos clientes cases é evidenciar nossa missão na prática.<br>
								Agrademos a participação, confiança, parceria de todos, principalmente por representarem os nossos valores.</h4>
					</div>
				</div>

				<div class="row">
					<?php
					$WP_cases_filtro = array(
							'post_type' => 'cases',
							'showposts' => -1,
							'orderby'   => 'date',
							'order'     => 'DESC'
						);

					$WP_cases = new WP_Query($WP_cases_filtro);

					if ( $WP_cases->have_posts() ) {
						while ( $WP_cases->have_posts() ) {
							$WP_cases->the_post();

							$imagem_cartola = wp_get_attachment_image_src( get_field('imagem_cartola', $post->ID), 'solucoes_case');
							$texto_cartola = get_field('texto_cartola');

							$clientes = wp_get_post_terms( $post->ID, 'clientes' );
							$cliente_logo = '';
							if( is_array( $clientes ) && count( $clientes ) > 0 ) {
								$cliente_logo = wp_get_attachment_image_src( get_field('logo_branco', 'clientes_' . $clientes[0]->term_id), 'full' );
								$cliente_logo = $cliente_logo[0];
							}

							?>
							<a href="<?php the_permalink(); ?>" class="col-lg-6 pic-wrapper">
								<div class="pic">
									<img src="<?php echo $imagem_cartola[0]; ?>" alt="">
									<div class="overlay">
										<h3>Qual era a necessidade?</h3>
										<p><?php echo $texto_cartola; ?></p>
									</div>
								</div>
								<?php
								if( $cliente_logo ) {
									?>
									<span class="caption caption-logo">
										<span class="caption-inner">
											<span class="logo-cliente valign">
												<img src="<?php echo $cliente_logo; ?>" alt="" class="center"><i></i>
											</span>
											<i class="icon icon-arrow-right"></i>
										</span>
									</span>
									<?php
								}
								?>
							</a>
							<?php
						}
					}

					wp_reset_postdata();
					?>

				</div>
			</div>
		</div>

		<div class="clear"></div>
	</div>
</main>

<?php get_footer(); ?>