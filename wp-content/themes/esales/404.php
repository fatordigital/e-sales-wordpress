<?php get_header(); ?>


  <main>
    <div class="hero">
      <div class="overlay">
        <div class="container valign">
          <div class="center">
            <h2 class="tit-esales">Página não encontrada!</h2>
            <h3 style="color:#fff; padding:20px;">Verifique a digitação do endereço ou navegue pelo menu acima.</h3>
          </div><i></i>
        </div>
      </div>
      <div class="bg" style="background-color:#666;"></div>
    </div>
    <div class="main-content sec-institucional">
      <div class="bg-esq"></div>
      <div class="bg-dir"></div>
      <div class="bg-center">
        <div class="bg-center-inner">
          <svg class="img" viewBox="0 0 1920 657" style="background-color:#ffffff00" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xml:space="preserve" x="0px" y="0px" width="1920px" height="657px">
            <path class="shape-bg" d="M 0 0 L 523 57 L 960 2.7641 L 1397 57 L 1920 3.191 L 1920 657 L 0 657 L 0 0 Z" fill="#ffffff" />
          </svg>
        </div>
      </div>
      <div class="cf">
        <div class="container">
          <div class="row">
            <div class="col-lg-10 center-block intro">

            </div>
          </div>
        </div>
      </div>
      <div class="clear"></div>
    </div>
  </main>
<?php get_footer(); ?>