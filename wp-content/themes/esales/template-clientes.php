<?php
/*
 * Template Name: Clientes
 */
?>
<?php get_header(); ?>

<main>
	<?php
	while(have_posts()) {
		the_post();

		$img = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' );

		?>
		<div class="hero">
			<div class="overlay">
				<div class="container valign">
					<div class="center">
						<h2>Clientes</h2>
					</div><i></i>
				</div>
			</div>

			<div class="bg" style="background-image: url(<?php echo $img[0]; ?>);"></div>
		</div>
		<?php
	}
	?>

  <div class="main-content">
    <div class="bg-esq"></div>
    <div class="bg-dir"></div>
    <div class="bg-center">
      <div class="bg-center-inner">
        <svg class="img" viewBox="0 0 1920 657" style="background-color:#ffffff00" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xml:space="preserve" x="0px" y="0px" width="1920px" height="657px">
          <path class="shape-bg" d="M 0 0 L 523 57 L 960 2.7641 L 1397 57 L 1920 3.191 L 1920 657 L 0 657 L 0 0 Z" fill="#ffffff" />
        </svg>
      </div>
    </div>
    <div class="sec-clientes">
      <div class="container">
        <div class="row">
          <div class="col-lg-10 center-block intro">
            <h1>Eficiência e transparência na comunicação</h1>
						<?php the_content(); ?>
          </div>
        </div>

        <div class="lista-logos-clientes">
          <ul>
						<?php
						$taxonomies = array(
						    'clientes'
						);

						$args = array(
						    'orderby'           => 'name',
						    'order'             => 'ASC',
						    'hide_empty'        => false,
						    'pad_counts'        => false
						);

						$clientes = get_clientes();

						if( is_array( $clientes ) && count( $clientes ) > 0 ) {

							foreach ($clientes as $item) {
								$cliente_logo = wp_get_attachment_image_src( get_field('logo_listagem', 'clientes_' . $item->term_id), 'full' );
								$cliente_logo_branco = wp_get_attachment_image_src( get_field('logo_branco', 'clientes_' . $item->term_id), 'full' );
								if($item->count_cases) { //IS CASE
									$WP_case_filtro = array(
		                  'post_type' => 'cases',
		                  'showposts' => 1,
		                  'orderby'   => 'date',
		                  'order'     => 'DESC',
		                  'tax_query' => array(
		                    array(
		                      'taxonomy' => 'clientes',
		                      'field'    => 'term_id',
		                      'terms'    => $item->term_id
		                    )
		                  )
		                );

		              $WP_case = new WP_Query($WP_case_filtro);

		              if ( $WP_case->have_posts() ) {
		                while ( $WP_case->have_posts() ) {
		                  $WP_case->the_post();

		                  $case_link = get_permalink();
		                }
		              }

		              wp_reset_postdata();
									?>
									<li class="case-suc">
			              <a href="<?php echo $case_link; ?>">
			                <img src="<?php echo $cliente_logo[0]; ?>" alt="" class="logo-c">
			                <img src="<?php echo $cliente_logo_branco[0]; ?>" alt="" class="logo-h">
			                <h3><br>Case de sucesso</h3>
			              </a>
			            </li>
									<?php
								} else {
									?>
									<li>
			              <a href="javascript:;">
			                <img src="<?php echo $cliente_logo[0]; ?>" alt="">
			              </a>
			            </li>
									<?php
								}
							}
						}
						?>
          </ul>
        </div>

      </div>
    </div>

    <div class="clear"></div>

    <div class="sec-depoimentos">
      <div class="container">
        <h1>Depoimentos</h1>
        <div class="depoimentos-wrapper">
          <div class="depoimentos">

          	<?php
          	$WP_depos_filtro = array(
          			'post_type' => 'depoimentos',
          			'showposts' => -1,
          			'orderby'   => 'date',
          			'order'     => 'DESC'
          		);

          	$WP_depos = new WP_Query($WP_depos_filtro);

          	if ( $WP_depos->have_posts() ) {
          		while ( $WP_depos->have_posts() ) {
          			$WP_depos->the_post();

        				$autor = get_field('autor');
        				$cargo = get_field('funcao');

        				$clientes = wp_get_post_terms( $post->ID, 'clientes' );
								$cliente_nome = '';
								if( is_array( $clientes ) && count( $clientes ) > 0 ) {
									$cliente_nome = $clientes[0]->name;
								}

          			?>
          			<div class="depoimentos-item valign">
		              <div class="center">
                  <div class="texto-dep"><?php the_content(); ?></div>
		                <h3><?php echo $autor; ?></h3>
		                <h4><?php echo $cargo; ?></h4>
		                <?php
		                if( $cliente_nome ) {
			                ?>
			                <h5><?php echo $cliente_nome; ?></h5>
			                <?php
		                }
		                ?>
		              </div><i></i>
		            </div>
          			<?php
          		}
          	}

          	wp_reset_postdata();
          	?>

          </div>
          <div class="clear"></div>

          <div class="dots"></div>
        </div>
      </div>
    </div>
  </div>
</main>

<?php get_footer(); ?>