<?php
/*
 * Template Name: Contato
 */
?>
<?php
$destinatarios = get_field('destinatarios');
$agradecimento_titulo = get_field('agradecimento_titulo');
$agradecimento_texto = get_field('agradecimento_texto');

if( is_post() ) {
	header('Cache-Control: no-cache, must-revalidate');
	header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
	header('Content-type: application/json');

	$post_fc_nome = $_POST['fc_nome'];
	$post_fc_email = $_POST['fc_email'];
	$post_fc_fone = $_POST['fc_fone'];
	$post_fc_segmento = $_POST['fc_segmento'];
	$post_fc_mensagem = $_POST['fc_mensagem'];


	$template = '	<p><strong>Nome: </strong> ' . $post_fc_nome . '</p>
								<p><strong>Email: </strong> ' . $post_fc_email . '</p>
								<p><strong>Fone: </strong> ' . $post_fc_fone . '</p>
								<p><strong>Segmento: </strong> ' . $post_fc_segmento . '</p>
								<p><strong>Mensagem: </strong> ' . $post_fc_mensagem . '</p>';


	$headers[] = 'Content-type:text/html;';
	$headers[] = 'Reply-to: ' . $post_fc_email;

	$envio = wp_mail( $destinatarios, '[E-Sales] Contato pelo Site', utf8_decode($template), $headers );


	echo json_encode( array( 'status' => 1, 'mensagem' => 'Envio ok', 'destino' => $destinatarios ) );

	exit();
}
?>

<?php get_header(); ?>


	<?php
	while(have_posts()) {
		the_post();

		$img = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' );

		?>
    <main>
		<div class="hero">
			<div class="overlay">
				<div class="container valign">
					<div class="center">
						<h2>Fale Conosco</h2>
					</div><i></i>
				</div>
			</div>

			<div class="bg" style="background-image: url(<?php echo $img[0]; ?>);"></div>
		</div>
		<?php

	}
	?>

  <div class="main-content">
    <div class="bg-esq"></div>
    <div class="bg-dir"></div>
    <div class="bg-center">
      <div class="bg-center-inner">
        <svg class="img" viewBox="0 0 1920 657" style="background-color:#ffffff00" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xml:space="preserve" x="0px" y="0px" width="1920px" height="657px">
          <path class="shape-bg" d="M 0 0 L 523 57 L 960 2.7641 L 1397 57 L 1920 3.191 L 1920 657 L 0 657 L 0 0 Z" fill="#ffffff" />
        </svg>
      </div>
    </div>
    <div class="sec-fale-conosco">
      <div class="container">
        <div class="row">
          <div class="col-lg-8 form-fale-conosco">
            <h2>Dúvidas, sugestões, reclamações, deixe seu contato que retornaremos o mais breve possível.</h2>

            <div class="loader" id="loader-contato">
                <img src="http://esales.dev/wp-content/themes/esales/img/carregando.gif">
            </div>

            <form action="<?php the_permalink(); ?>" method="post" data-ajax="form_padrao" id="form_contato">
              <div class="fieldset">
                <div class="field-group-double cf">
                  <div class="field">
                    <input type="text" name="fc_nome" id="fc_nome" placeholder="Nome" class="form-control" data-req="required">
                  </div>
                  <div class="field">
                    <input type="email" name="fc_email" id="fc_email" placeholder="E-mail" class="form-control" data-req="email">
                  </div>
                </div>
                <div class="field-group-double cf">
                  <div class="field">
                    <input type="text" name="fc_fone" id="fc_fone" placeholder="Telefone" class="form-control" data-req="required" data-mask="(99) 99999-9999">
                  </div>
                  <div class="field">
                    <select name="fc_segmento" id="fc_segmento" data-req="required">
                      <option value="">Segmento</option>
                      <?php
                      $args = array(
												    'orderby'           => 'name',
												    'order'             => 'ASC',
												    'hide_empty'        => false,
												    'parent'            => 0,
												    'fields'            => 'all'
													);

											$terms = get_terms('segmentos', $args);

											if( is_array( $terms ) && count( $terms ) > 0 ) {
												foreach ($terms as $item) {
													?>
													<option value="<?php echo $item->name; ?>"><?php echo $item->name; ?></option>
													<?php
												}
											}
                      ?>
                    </select>
                  </div>
                </div>
                <div class="field">
                  <textarea name="fc_mensagem" id="fc_mensagem" cols="30" rows="5" placeholder="Mensagem" class="form-control" data-req="required"></textarea>
                </div>
                <div class="field-group-double cf">
                  <div class="field">
                    <button type="submit" class="bt-padrao" data-href="Enviar" >Enviar</button>
                  </div>
                  <div class="field">
                    <p class="form-error-message" style="display:none;">Todos os campos sinalizados são de preenchimento obrigatório</p>
                  </div>
                </div>
              </div>
            </form>

            <div id="agradecimento-contato" style="display:none;">
              <div class="box-agradecimento">
                <h1>Obrigado por entrar em contato!</h1>
                <p>Agradecemos seu interesse em nossa empresa, em breve nossa equipe fará contato.<br>
										Aproveite para conhecer nossos demais cases e produtos navegando pelo site.<br>
										Compartilhe sua opinião conosco. Nos acompanhe nas redes sociais e em nosso blog.</p>
              </div>
              <a href="<?php echo home_url(); ?>" class="bt-padrao">Ir para página inicial</a>
            </div>

          </div>

          <div class="col-lg-4 lat-trabalhe">
            <h3>Trabalhe Conosco</h3>
            <p>Venha fazer parte desta equipe tão especial. Trabalhe em um lugar agradável, harmonioso, cheio de gente feliz, bonita e realizada!<br>
							Envie seu currículo para <a href="mailto:rh@esales.com.br">rh@esales.com.br</a></p>
            <a href="https://www.linkedin.com/company/e-sales-solu-es-de-integra-o" class="bt-padrao" target="_blank">Confira as vagas no Linkedin.</a>
          </div>

          <div class="clear"></div>
        </div>
      </div>
    </div>
  </div>
</main>

<?php get_footer(); ?>