<?php
/*
 * Template Name: Produtos
 */
?>
<?php get_header(); ?>

<?php
while( have_posts() ) {
  the_post();

  $img_cartola = wp_get_attachment_image_src( get_field('imagem_cartola'), 'full' );
  $img_logo = wp_get_attachment_image_src( get_field('logo_cartola'), 'full' );

  $topicos = get_field('topicos'); //titulo, texto

  $post_type_produtos = get_field('tipo_produto');

  $titulo = get_the_title();
}
?>
<main>
  <div class="hero">
    <div class="overlay">
      <div class="container valign">
        <div class="center">
          <h2><?php echo $titulo; ?></h2>
          <?php
          if( is_array( $img_logo ) && $img_logo[0] ) {
            ?>
            <div class="logo-solucao">
              <img src="<?php echo $img_logo[0]; ?>" alt="">
            </div>
            <?php
          }
          ?>

        </div><i></i>
      </div>
    </div>

    <div class="bg" style="background-image: url(<?php echo $img_cartola[0]; ?>);"></div>
  </div>

  <div class="main-content">
    <div class="bg-esq"></div>
    <div class="bg-dir"></div>
    <div class="bg-center">
      <div class="bg-center-inner">
        <svg class="img" viewBox="0 0 1920 657" style="background-color:#ffffff00" version="1.1"
          xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xml:space="preserve"
          x="0px" y="0px" width="1920px" height="657px">
          <path class="shape-bg" d="M 0 0 L 523 57 L 960 2.7641 L 1397 57 L 1920 3.191 L 1920 657 L 0 657 L 0 0 Z" fill="#ffffff"/>
        </svg>
      </div>
    </div>

    <div class="sec-unidade">
      <div class="container cf">
        <?php
        $topicos_row = array_chunk($topicos, 2);
        foreach ($topicos_row as $row) {
          ?>
          <div class="row">
            <?php
            foreach ($row as $topico) {
              ?>
              <div class="col-lg-6">
                <h2><?php echo $topico['titulo']; ?></h2>
                <p><?php echo $topico['texto']; ?></p>
              </div>
              <?php
            }
            ?>
          </div>
          <?php
        }
        ?>
      </div>
    </div>

    <div class="sec-outras-unidades">
      <div class="container">
        <div class="row align-center">
          <div class="col-lg-12">
            <h2>Produtos e serviços</h2>
          </div>
        </div>

        <div class="lista-unidades">
          <ul>
            <?php
            $WP_produtos_filtro = array(
                'post_type' => $post_type_produtos,
                'showposts' => -1,
                'orderby'   => 'date',
                'order'     => 'DESC'
              );

            $WP_produtos = new WP_Query($WP_produtos_filtro);

            if ( $WP_produtos->have_posts() ) {
              while ( $WP_produtos->have_posts() ) {
                $WP_produtos->the_post();
                ?>
                 <li>
                  <a href="<?php the_permalink(); ?>" class="item valign">
                    <span class="center"><?php the_title(); ?></span><i></i>
                    <b class="icon icon-arrow-right"></b>
                  </a>
                </li>
                <?php
              }
            }

            wp_reset_postdata();
            ?>
          </ul>
        </div>
      </div>
    </div>
  </div>
</main>

<?php get_footer(); ?>