<?php
define('WP_USE_THEMES', false);
require_once('../../../wp-load.php');

header('Cache-Control: no-cache, must-revalidate');
header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
header('Content-type: application/json');

$retorno = array( 'status' => 0, 'mensagem' => 'Erro' );

if( is_post() ) {

	$post_nome = $_POST['nome'];
	$post_email = $_POST['email'];


	$rd_data = array(
		'nome' => $post_nome,
		'email' => $post_email
	);

	$RD = new RD_Station( $rd_data );
	$RD->token = '0c29a0fc488e0e5827165c65e2b6006f';
	$RD->identifier = 'site e-Sales';
	$RD->createLead();

	$retorno = array( 'status' => 1, 'mensagem' => 'Enviado' );

}

echo json_encode( $retorno );