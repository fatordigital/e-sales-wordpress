<?php
/*
 * Template Name: Institucional
 */
?>
<?php get_header(); ?>

<?php
while(have_posts()) {
	the_post();

	$visao_titulo = get_field('visao_titulo');
	$visao_texto = get_field('visao_texto');

	$missao_titulo = get_field('missao_titulo');
	$missao_texto = get_field('missao_texto');

	$valores_titulo = get_field('valores_titulo');
	$valores_texto = get_field('valores_texto');

	$ce_titulo = get_field('ce_titulo');
	$ce_texto = get_field('ce_texto');
	$ce_imagem = get_field('ce_imagem');
	$ce_arquivo = get_field('ce_arquivo');

	$ps_titulo = get_field('ps_titulo');
	$ps_texto = get_field('ps_texto');

	$ps_projetos = get_field('ps_projetos'); //imagem, titulo, texto
	$ps_associacoes = get_field('ps_associacoes'); //imagem, titulo, texto

	$pa_titulo = get_field('pa_titulo');
	$pa_texto = get_field('pa_texto');

	$pa_projetos = get_field('pa_projetos'); //titulo, texto, imagem_1, imagem_2, imagem_3, imagem_4

	$pdc_titulo = get_field('pdc_titulo');
	$pdc_texto = get_field('pdc_texto');

	$tc_titulo = get_field('tc_titulo');
	$tc_texto = get_field('tc_texto');


	$historia = get_field('linha_tempo');

	$img = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' );

	?>
	<main>

    <div class="hero">
      <div class="overlay">
        <div class="container valign">
          <div class="center">
            <h2 class="tit-esales">e-SALES</h2>
          </div><i></i>
        </div>
      </div>
      <div class="bg" style="background-image: url(<?php echo $img[0]; ?>);"></div>
    </div>

        <div class="main-content sec-institucional">
          <div class="bg-esq"></div>
          <div class="bg-dir"></div>
          <div class="bg-center">
            <div class="bg-center-inner">
              <svg class="img" viewBox="0 0 1920 657" style="background-color:#ffffff00" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xml:space="preserve" x="0px" y="0px" width="1920px" height="657px">
                <path class="shape-bg" d="M 0 0 L 523 57 L 960 2.7641 L 1397 57 L 1920 3.191 L 1920 657 L 0 657 L 0 0 Z" fill="#ffffff" />
              </svg>
            </div>
          </div>

          <div class="cf">

            <div class="container">
              <div class="row">
                <div class="col-lg-10 center-block intro">
                  <h1>Tecnologia inovadora e excelência no atendimento</h1>
                </div>
              </div>
            </div>

            <div class="col-left-full">
              <div class="col-left-full-inner">
                <?php the_content(); ?>
              </div>
            </div>
            <div class="col-right-full">
              <div class="box-visao cf">
                <div class="content">
                  <div class="item ico-visao">
                    <h3><?php echo $visao_titulo; ?></h3>
                    <p><?php echo $visao_texto; ?></p>
                  </div>
                  <div class="item ico-missao">
                    <h3><?php echo $missao_titulo; ?></h3>
                    <p><?php echo $missao_texto; ?></p>
                  </div>
                  <div class="item ico-valores">
                    <h3><?php echo $valores_titulo; ?></h3>
                    <?php echo $valores_texto; ?>
                  </div>
                </div>
              </div>
            </div>

          </div>


          <?php
          if( is_array( $historia ) && count( $historia ) > 0 ) {

          	?>
          	<div class="clear"></div>

	         	<div class="sec-historia cf">
	         		<div class="container">
	         			<h3>História</h3>

	         			<ul>
		          	<?php
		          	foreach ($historia as $item) {
                  $css_branco = intval( $item['ano'] ) % 5 == 0 ? 'branco' : '';
                  $css_prala = intval( $item['ano'] ) % 2 == 0 ? 'textoalinha' : '';
		          		?>

		          		<li class="<?php echo $css_prala; ?>">
                    <div class="texto praca">
                      <strong><?php echo $item['ano']; ?></strong><br><?php echo $item['texto']; ?>
                    </div>
		       					<div class="barra <?php echo $css_branco; ?>"></div>
		       					<div class="texto prala">
		       						<strong><?php echo $item['ano']; ?></strong><br><?php echo $item['texto']; ?>
		       					</div>
		       				</li>
		          		<?php
		          	}
		          	?>
		          	</ul>
	         		</div>
	         	</div>
          	<?php

          }
          ?>

          <div class="clear"></div>

					<?php
					if( $ce_arquivo ) {
						$ce_imagem_src = wp_get_attachment_image_src( $ce_imagem, 'full' );
						?>

	          <div class="sec-codigo cf">
	            <div class="container">
	              <div class="row">
	                <div class="col-lg-3">
	                  <img src="<?php echo $ce_imagem_src[0]; ?>" alt="">
	                  <a href="<?php echo $ce_arquivo; ?>" class="bt-download-codigo">
	                    <span class="item-inner">
	                      <h3>Download<br>do código</h3>
	                      <i class="icon icon-arrow-right"></i>
	                    </span>
	                  </a>
	                </div>
	                <div class="col-lg-9">
	                  <h2><?php echo $ce_titulo; ?></h2>
	                  <?php echo $ce_texto; ?>
	                </div>
	              </div>
	            </div>
	          </div>

          	<?php
          }
          ?>

          <div class="sec-projetos-sociais cf">
            <div class="container">
              <div class="row">
                <div class="col-lg-9 center-block intro">
                  <h2><?php echo $ps_titulo; ?></h2>
                  <p><?php echo $ps_texto; ?></p>
                </div>

                <div class="lista-projetos-sociais">
      						<?php
      						if( is_array( $ps_projetos ) && count( $ps_projetos ) > 0 ) {
      							foreach ($ps_projetos as $projeto) {
      								$img_src = wp_get_attachment_image_src( $projeto['imagem'], 'full' );
      								?>
											<div class="item col-lg-4">
		                    <img src="<?php echo $img_src[0]; ?>" alt="">
		                    <h3><?php echo $projeto['titulo']; ?></h3>
		                    <p><?php echo $projeto['texto']; ?></p>
		                  </div>
      								<?php
      							}
      						}
      						?>
                </div>
                <div class="fundo-cinza">
  								<div class="col-lg-9 center-block intro">
                    <h2>Associações de Classe</h2>
                  </div>

                  <div class="lista-projetos-sociais-fixo">
                  	<?php
                  	if( is_array( $ps_associacoes ) && count( $ps_associacoes ) > 0 ) {
                  		foreach ($ps_associacoes as $assoc) {
                  			$img_src = wp_get_attachment_image_src( $assoc['imagem'], 'full' );
                  			?>
  											<div class="item col-lg-6">
  		                    <img src="<?php echo $img_src[0]; ?>" alt="">
  		                    <h3><?php echo $assoc['titulo']; ?></h3>
  		                    <p><?php echo $assoc['texto']; ?></p>
  		                  </div>
                  			<?php
                  		}
                  	}
                  	?>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="sec-projetos-ambientais cf">
            <div class="container">
              <div class="row">
                <div class="col-lg-9 center-block intro">
                  <h2><?php echo $pa_titulo; ?></h2>
                  <p><?php echo $pa_texto; ?></p>
                </div>
              </div>
            </div>

						<?php
						if( is_array( $pa_projetos ) && count( $pa_projetos ) > 0 ) {
							foreach ($pa_projetos as $projeto) {

								$img_1_src = wp_get_attachment_image_src( $projeto['imagem_1'], 'full' );
								$img_2_src = wp_get_attachment_image_src( $projeto['imagem_2'], 'full' );
								$img_3_src = wp_get_attachment_image_src( $projeto['imagem_3'], 'full' );
								$img_4_src = wp_get_attachment_image_src( $projeto['imagem_4'], 'full' );
								?>
								<div class="col-left-full">
		              <div class="col-left-full-inner">
		                <h3><?php echo $projeto['titulo']; ?></h3>
		                <?php echo $projeto['texto']; ?>
		              </div>
		            </div>
		            <div class="col-right-full">
		              <div class="bg-dir"></div>
		              <?php
		              if( is_array($img_1_src) && $img_1_src[0] ) {
		              	?>
										<img src="<?php echo $img_1_src[0]; ?>" alt="">
		              	<?php
		              }
		              ?>

		              <?php
		              if( is_array($img_2_src) && $img_2_src[0] ) {
		              	?>
										<img src="<?php echo $img_2_src[0]; ?>" alt="">
		              	<?php
		              }
		              ?>

		              <div class="clear"></div>
		              <?php
		              if( is_array($img_3_src) && $img_3_src[0] ) {
		              	?>
										<img src="<?php echo $img_3_src[0]; ?>" alt="">
		              	<?php
		              }
		              ?>

		              <?php
		              if( is_array($img_4_src) && $img_4_src[0] ) {
		              	?>
										<img src="<?php echo $img_4_src[0]; ?>" alt="">
		              	<?php
		              }
		              ?>
		            </div>
								<?php
							}
						}
						?>

          </div>

          <div class="sec-projetos-treina cf">
            <div class="col-left-full">
              <div class="col-left-full-inner">
                <h3><?php echo $pdc_titulo; ?></h3>
                <p><?php echo $pdc_texto; ?></p>
              </div>
            </div>
            <div class="col-right-full">
              <div class="col-right-full-inner">
                <h3><?php echo $tc_titulo; ?></h3>
                <p><?php echo $tc_texto; ?></p>
              </div>
            </div>
          </div>
        </div>
      </main>
	<?php

}
?>

<?php get_footer(); ?>