<?php
  header('Access-Control-Allow-Origin: *');
  sleep(1);
?>
<div class="bg-esq"><div class="tip"></div></div>
<div class="bg-dir" style="background-image: url(<?php bloginfo( 'template_url' ); ?>/img/_foto_paqueta.jpg);"></div>

<div class="container">
  <div class="row content">
    <div class="col-lg-6 col-txt">
      <p><?php echo rand(); ?> Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.</p>

      <ul>
        <li><span>Catálogo de Produtos</span></li>
        <li><span>Catálogo de Produtos</span></li>
        <li><span>Controle de Entregas Mobile</span></li>
        <li><span>Agendamento de Entregas</span></li>
        <li><span>Boletos de Cobrança</span></li>
        <li><span>Recebimento Documentos Fiscais</span></li>
        <li><span>EDI Varejo</span></li>
      </ul>
    </div>

    <div class="col-lg-6 col-cta valign">
      <div class="center">
        <div class="veja-solucao">
          <p>Veja a solução utilizada para</p>
          <img src="img/logo-paqueta.png" alt="Paquetá">
        </div>

        <div class="link-detalhe">
          <p>Acesse o site da OobJ <br>
          e descubra todos os detalhes <br>
          utilizados para esta solução.</p>
          <a href="#" class="bt-cta"><img src="img/logo-oobj-sm.png" alt="Oobj"></a>
        </div>
      </div><i></i>
    </div>
  </div>
</div>