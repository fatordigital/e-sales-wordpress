<?php
require_once('lib/class.RD_Station.php');


show_admin_bar( false );
add_theme_support( 'post-thumbnails' );

add_image_size( 'solucoes_case', 570, 485, true );
add_image_size( 'solucoes_solucoes', 470, 115, true );
add_image_size( 'produtos_solucoes', 370, 115, true );
add_image_size( 'cases_depoimentos', 610, 460, true );

function ativo($bool) {
	if( $bool ) {
		echo 'class="ativo"';
	}
}

function is_post() {
	return $_SERVER['REQUEST_METHOD'] == 'POST';
}

function get_share_links( $post_id = 0 ) {
	$pid = intval( $post_id ) > 0 ? $post_id : get_the_ID();

	/*
	$('a.bt-share').on('click', function(){
		var url = $(this).attr('href');
		window.open(url,'pop','width=450, height=450, top=100, left=100, scrollbars=no');

		return false;
	});
	*/

	$title = get_the_title( $pid );
	$link = get_permalink( $pid );

	$thumb_id = get_post_thumbnail_id( $pid );
	$thumb_src = wp_get_attachment_image_src( $thumb_id, 'full' );


	$retorno['facebook'] = 'http://www.facebook.com/share.php?u=' . urlencode( $link ) . '&t=' . urlencode( $title );
	$retorno['twitter'] = 'http://twitter.com/?status=' . urlencode( $title . ' ' . $link);
	$retorno['pinterest'] = 'http://pinterest.com/pin/create/button/?media=' . urlencode( $thumb_src[0] ) . '&url=' . urlencode( $link );
	$retorno['linkedin'] = 'http://www.linkedin.com/shareArticle?mini=true&url=' . urlencode( $link ) . '&title=' . urlencode($title) . '&source=' . urlencode(get_bloginfo('name'));
	$retorno['googleplus'] = 'https://plus.google.com/share?url=' . urlencode( $link );

	return $retorno;
}

function get_share_header() {
	?>
	<meta property="og:title" content="<?php wp_title( '|', true, 'right' ); bloginfo( 'name' ); ?>">
	<meta property="og:type" content="website">
	<meta property="og:site_name" content="<?php bloginfo( 'name' ); ?>">
	<?php
	if( is_singular() && !is_page() ) {
		$queried_object = get_queried_object();

		$share_post_thumb_id = get_post_thumbnail_id( $queried_object->ID );

		$share_post_thumb_src = wp_get_attachment_image_src( $share_post_thumb_id, 'full' );

		$share_post_thumb_src = is_array( $share_post_thumb_src ) ? $share_post_thumb_src[0] : get_bloginfo( 'template_url' ) . '/share.png';

		$share_post_thumb_info = getimagesize($share_post_thumb_src);

		$description = '';

		setup_postdata( $queried_object );
		$description = get_the_excerpt();

		if(!$description) {
			$description = get_bloginfo( 'description', 'display' );
		}
		?>
		<meta property="og:description" content="<?php echo $description; ?>">
		<meta property="og:url" content="<?php the_permalink(); ?>">
		<meta property="og:image" content="<?php echo $share_post_thumb_src; ?>">
		<meta property="og:image:width" content="<?php echo $share_post_thumb_info[0]; ?>">
		<meta property="og:image:height" content="<?php echo $share_post_thumb_info[1]; ?>">
		<?php
	} else {
		?>
		<meta property="og:description" content="<?php echo get_bloginfo( 'description', 'display' ); ?>">
		<?php
		if( is_home() ) {
			?>
			<meta property="og:url" content="<?php echo home_url(); ?>">
			<?php
		} else {
		?>
			<meta property="og:url" content="<?php the_permalink(); ?>">
			<?php
		}
		?>
		<meta property="og:image" content="<?php bloginfo( 'template_url' ); ?>/share.png">
		<meta property="og:image:width" content="1200">
		<meta property="og:image:height" content="630">
		<?php
	}
}

function sanitize_filename_on_upload($filename) {
	$ext = end(explode('.', $filename));
	$sanitized = sanitize_title( substr($filename, 0, -(strlen($ext)+1)) );
	return strtolower($sanitized.'.'.$ext);
}

add_filter('sanitize_file_name', 'sanitize_filename_on_upload', 10);

function get_clientes() {
	global $wpdb;

	$sql = 'SELECT T.term_id, T.name, T.slug, TT.taxonomy, TT.description,
			( SELECT COUNT(1)
			FROM wp_term_relationships AS TR
			INNER JOIN wp_posts AS P ON TR.object_id = P.id
			WHERE TR.term_taxonomy_id = TT.term_taxonomy_id AND P.post_type = \'cases\' AND P.post_status = \'publish\'
			 )  AS count_cases
			FROM wp_terms AS T
			INNER JOIN wp_term_taxonomy AS TT ON T.term_id = TT.term_id
			WHERE TT.taxonomy = \'clientes\'';

	$res = $wpdb->get_results( $sql );

	return $res;
}

function the_slug($echo=true){
  $slug = basename(get_permalink());
  do_action('before_slug', $slug);
  $slug = apply_filters('slug_filter', $slug);
  if( $echo ) echo $slug;
  do_action('after_slug', $slug);
  return $slug;
}