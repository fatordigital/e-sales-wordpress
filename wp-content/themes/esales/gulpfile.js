var gulp        = require('gulp');
var $           = require('gulp-load-plugins')();
var browserSync = require('browser-sync').create();


/*======================================
  SVG
======================================*/
gulp.task('svg2png', function() {
  var svg = gulp.src(['./img/svg/*.svg']);

  svg
    .pipe($.watch('./img/svg/*.svg', { base: './img/svg/' }))
    .pipe($.changed('./img', { extension: '.png' }))
    .pipe($.svg2png())
    .pipe(gulp.dest('./img'));

  svg
    .pipe($.watch('./img/svg/*.svg', { base: './img/svg/' }))
    .pipe($.changed('./img'))
    .pipe(gulp.dest('./img'));

  return svg;
});


/*======================================
  Iconfont
======================================*/
gulp.task('iconfont', function() {
  var fontName = 'Icons';

  return gulp.src(['./img/icons/*.svg'])
    .pipe($.iconfont({
      fontName: fontName,
      appendCodepoints: true,
      normalize: true
    }))
    .on('glyphs', function(glyphs, options) {
      gulp.src(['./sass/template/_icons.scss'])
        .pipe($.ejs({
          glyphs: glyphs,
          fontName: fontName,
          fontPath: 'fonts/',
          className: 'icon'
        }))
        .pipe($.rename({ extname: '.scss' }))
        .pipe(gulp.dest('./sass'));
    })
    .pipe(gulp.dest('./fonts'));
});


/*======================================
  SASS
======================================*/
gulp.task('sass', function() {
  return gulp.src(['./*.scss'])
    .pipe($.sourcemaps.init())
      .pipe(
        $.sass({
          precision: 10,
          outputStyle: 'expanded'
        })
        .on('error', $.notify.onError())
      )
      .pipe($.concat('style.css'))
      .pipe($.autoprefixer('last 2 versions', 'ie 8', 'ie 9', '> 1%'))
    .pipe($.sourcemaps.write('./'))
    .pipe(gulp.dest('./'))
    .pipe($.filter('*.css'))
    .pipe(browserSync.stream())
    .pipe($.rename({ suffix: '.min' }))
    .pipe($.csso())
    .pipe(gulp.dest('./'));
});


/*======================================
  Javascript
======================================*/
gulp.task('lint', function() {
  return gulp.src(['./js/main.js'])
    .pipe($.watch('./js/main.js'))
    .pipe($.plumber())
    .pipe($.jshint('.jshintrc'))
    .pipe($.jshint.reporter('jshint-stylish'));
});


/*======================================
  Templates
======================================*/
gulp.task('template', function() {
  return gulp.src(['./templates/*.ejs'])
    .pipe($.watch(['./templates/*.ejs'], { base: './templates/' }))
    .pipe($.ejs())
    .pipe(gulp.dest('./'));
});

gulp.task('template-all', function() {
  return gulp.src(['./templates/*.ejs'])
    .pipe($.ejs())
    .pipe(gulp.dest('./'));
});


/*======================================
  Live preview
======================================*/
gulp.task('serve', function() {
  browserSync.init({
    open: false,
    online: false,
    ghostMode: false,
    notify: false,
    server: {
      baseDir: './',
      directory: true
    }
  });
});


/*======================================
  Build
======================================*/
gulp.task('build', ['template', 'template-all', 'sass', 'lint', /*'compress',*/ 'svg2png'], function() {
  $.util.log($.util.colors.green('Build is finished'));
});


/*======================================
  Watch files
======================================*/
gulp.task('watch', ['serve'], function() {
  var watchOpts = {
        verbose: true,
        read: false
      };

  $.watch([
    './*.html',
    './js/*.js',
    './img/*.{jpg,png,svg,gif,webp,ico}'
  ], watchOpts, browserSync.reload);

  /* Watch HTML */
  $.watch(['./templates/partials/*.ejs'], watchOpts, function() {
    gulp.start('template-all');
  });

  /* Watch styles */
  $.watch(['./*.scss', './sass/**'], watchOpts, function() {
    gulp.start('sass');
  });

  /* Watch images */
  $.watch(['./img/icons/*.svg'], watchOpts, function() {
    gulp.start('iconfont');
  });

  /* Watch scripts */
  // $.watch(['./js/*.js'], watchOpts, function() {
  //   gulp.start('compress');
  // });
});


/*======================================
  Default task
======================================*/
gulp.task('default', ['build', 'watch']);