
<?php

// form envio 
	$destinatarios = "marketing@esales.com.br";
	//$destinatarios = "rosalba.monteiro@fatordigital.com.br";
	//$agradecimento_titulo = get_field('agradecimento_titulo');
	$agradecimento_texto = get_field('agradecimento_texto');

		if( is_post() ) {
		header('Cache-Control: no-cache, must-revalidate');
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
		header('Content-type: application/json');

		$post_fc_nome = $_POST['nome'];
		$post_fc_email = $_POST['email'];
		$post_fc_fone = $_POST['telefone'];
		$post_fc_pagina = $_POST['pagina'];
		$post_fc_mensagem = $_POST['mensagem'];

		include('rdstation.php');
		/*RdStation::addLeadConversionToRdstationCrm('esales-form-lead', array(
		    'email'     => $_POST['email'], 
		    'nome'      => $_POST['nome'],
		    'produtos'	=> $_POST['pagina']
		    )
		);*/
		RdStation::addLeadConversionToRdstationCrm($_POST['pagina'], array(
		    'email'     => $_POST['email'], 
		    'nome'      => $_POST['nome'],
		    'produtos'	=> $_POST['prd'],
		    'telefone'  => $_POST['telefone'],
		    'mensagem'  => $_POST['mensagem'],
		    )
		);

		$template = '	<p><strong>Nome: </strong> ' . $post_fc_nome . '</p>
									<p><strong>Email: </strong> ' . $post_fc_email . '</p>
									<p><strong>Fone: </strong> ' . $post_fc_fone . '</p>
									<p><strong>Segmento: </strong> ' . $post_fc_pagina . '</p>
									<p><strong>Mensagem: </strong> ' . $post_fc_mensagem . '</p>';


		$headers[] = 'Content-type:text/html;';
		$headers[] = 'Reply-to: ' . $post_fc_email;

		$envio = wp_mail( $destinatarios, '[E-Sales] Contato soluções', utf8_decode($template), $headers );


		echo json_encode( array( 'status' => 1, 'mensagem' => 'Envio ok', 'destino' => $destinatarios, 'minha_url' =>"/goal".$_SERVER['REQUEST_URI']) );

		exit();
	}
?>


<?php get_header(); ?>

<?php

while( have_posts() ) {
	the_post();

	//CARTOLA
	$chamada_conheca = get_field('chamada_conheca');
	$imagem_cartola = wp_get_attachment_image_src( get_field('imagem'), 'full' );
	$logo_cartola = wp_get_attachment_image_src( get_field('logo'), 'full' );

	//SOBRE
	$titulo_sobre = get_field('titulo_sobre');
	$texto_sobre = get_field('texto_sobre');

	//BOX PROBLEMA
	$titulo_problema = get_field('titulo_problema');
	$texto_problema = get_field('texto_problema');

	//BOX BENEFICIOS
	$titulo_beneficios = get_field('titulo_beneficios');
	$items_benefícios = get_field('items_benefícios'); //texto

	//BOX ATRIBUTOS
	$titulo_atributos = get_field('titulo_atributos');
	$items_atributos = get_field('items_atributos'); //texto

	//CONTATO
	$titulo_contato = get_field('titulo_contato');
	$nome_rd_station = get_field('nome_rd_station');
	$codigo_rd_station = get_field('codigo_rd_station');

	//CASES SOLUCOES
	$case_lista = get_field('case');
	$solucoes_lista = get_field('solucoes');
	$produtos_lista = get_field('produtos');

	//CARACTERISTICAS
	$caracteristicas = get_field('produtos_home');

	//SHARE
	$share_links = get_share_links();

	?>

	<main>
		<div class="hero">
			<div class="overlay">
				<div class="container valign">
					<div class="center">
						<h2><?php the_title(); ?></h2>
						<div class="logo-solucao">
							<img src="<?php echo $logo_cartola[0]; ?>" alt="<?php the_title(); ?>">
						</div>
					</div><i></i>
				</div>
			</div>

			<div class="bg" style="background-image: url(<?php echo $imagem_cartola[0]; ?>);"></div>
		</div>

		<div class="main-content">
			<div class="bg-esq"></div>
			<div class="bg-dir"></div>
			<div class="bg-center">
				<div class="bg-center-inner">
					<svg class="img" viewBox="0 0 1920 657" style="background-color:#ffffff00" version="1.1"
						xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xml:space="preserve"
						x="0px" y="0px" width="1920px" height="657px">
						<path class="shape-bg" d="M 0 0 L 523 57 L 960 2.7641 L 1397 57 L 1920 3.191 L 1920 657 L 0 657 L 0 0 Z" fill="#ffffff"/>
					</svg>
				</div>
			</div>

			<div class="sec-solucoes cf">
				<div class="container">
					<div class="row">
						<div class="col-lg-10 center-block intro">
							<h1><?php echo $titulo_sobre; ?></h1>
							<?php echo $texto_sobre; ?>
						</div>
					</div>
				</div>
			</div>			
			<div class="sec-solucoes cf">
				<!--- formulario -->
				<div class="sec-form">
					<div class="container">
						<div class="row">
							<h2>Entre em contato sobre essa solução</h2>							
							<form action="<?php the_permalink(); ?>" method="post" data-ajax="form_padrao" id="form_sol">
								<div class="div-metade right">
									<div class="form-group field">
										<input type="text" class="form-control" placeholder="Nome" data-req="required" name="nome" >
									</div>
									<div class="form-group field">
										<input type="email" class="form-control" placeholder="E-mail" name="email"  data-req="required">
									</div>
									<div class="form-group field">
										<input type="tel" class="form-control" placeholder="Telefone" name="telefone"  data-req="required" data-mask="(99) 99999-9999">
									</div>
									<div class="form-group ">
										<input type="hidden" class="form-control" value="<?php the_slug(); ?>" name="pagina" >
										<input type="hidden" class="form-control" value="<?php the_title(); ?>" name="prd" >
									</div>
								</div>
								<div class="div-metade left">
									<div class="form-group field">
										<textarea class="form-control" rows="7" placeholder="Mensagem" name="mensagem"  data-req="required"></textarea>
									</div>
									<div class="form-group field">
										<button class="btn btn-white" data-href="Enviar" >Enviar</button>
									</div>
								</div>
							</form>
							<div id="agradecimento-contato" style="display:none;">
					              <div class="box-agradecimento">
					                <h1>Obrigado por entrar em contato!</h1>
					                <p>Agradecemos seu interesse em nossa empresa, em breve nossa equipe fará contato.<br>
															Aproveite para conhecer nossos demais cases e produtos navegando pelo site.<br>
															Compartilhe sua opinião conosco. Nos acompanhe nas redes sociais e em nosso blog.</p>
					              </div>
					              <a href="<?php echo home_url(); ?>" class="bt-padrao">Ir para página inicial</a>
				            </div>
						</div>
					</div>
				</div>
				<!--- formulario -->
				<div class="col-left-full">
					<div class="box-problema cf">
						<div class="content">
							<h2><?php echo $titulo_problema; ?></h2>
							<p><?php echo $texto_problema; ?></p>
						</div>
					</div>

					<div class="box-lista-solucoes">
						<?php
						//$caracteristicas = wp_get_post_terms( $post->ID, 'caracteristicas' );
						if( is_array( $caracteristicas ) && count( $caracteristicas ) > 0 ) {
							?>
							<ul>
								<?php
								foreach ($caracteristicas as $carac) {

									$link = get_permalink( $carac );
									$title = get_the_title( $carac );
									$imagem = wp_get_attachment_image_src( get_field('icone_caracteristica', $carac), 'full'  );

									/*$img = wp_get_attachment_image_src( get_field('imagem', 'caracteristicas_' . $carac->term_id), 'full' );
									$prod_relacionado = get_field('produto', 'caracteristicas_' . $carac->term_id);
									if( is_array( $prod_relacionado ) && count( $prod_relacionado ) > 0 ) {
										$prod_rel_link = get_permalink( $prod_relacionado[0] );
									} else {
										$prod_rel_link = '#';
									}*/
									?>
									<li>
										<a href="<?php echo $link; ?>">
											<span><?php echo $title; ?></span>
										</a>
										<div class="icon-wrapper">
											<a href="<?php echo $link; ?>">
												<img src="<?php echo $imagem[0]; ?>" alt="">
											</a>
										</div>
									</li>
									<?php
								}
								?>
							</ul>
							<?php
						}
						?>
					</div>
				</div>

				<div class="col-right-full">
					<div class="col-right-full-inner">
						<h3><?php echo $titulo_beneficios; ?></h3>
						<ul>
							<?php
							if( is_array( $items_benefícios ) && count( $items_benefícios ) > 0 ) {
								foreach ($items_benefícios as $item) {
									?>
									<li><span><?php echo $item['texto']; ?></span></li>
									<?php
								}
							}
							?>
						</ul>

						<div class="sep"></div>

						<h3><?php echo $titulo_atributos; ?></h3>
						<ul>
							<?php
							if( is_array( $items_atributos ) && count( $items_atributos ) > 0 ) {
								foreach ($items_atributos as $item) {
									?>
									<li><span><?php echo $item['texto']; ?></span></li>
									<?php
								}
							}
							?>
						</ul>
					</div>
				</div>
			</div>

			<div class="clear"></div>

			<?php get_template_part('inc-sec-contato'); ?>

			<div class="sec-outro">
				<div class="container">
					<div class="row">
						<div class="col-lg-6">
							<?php
							if( is_array( $case_lista ) && count( $case_lista ) > 0 ) {
								$case = $case_lista[0];

								$texto_cartola = get_field('texto_cartola', $case->ID);
								$imagem_cartola = wp_get_attachment_image_src( get_field('imagem_cartola', $case->ID), 'solucoes_case');

								$clientes = wp_get_post_terms( $case->ID, 'clientes' );
								$cliente_logo = '';
								if( is_array( $clientes ) && count( $clientes ) > 0 ) {
									$cliente_logo = wp_get_attachment_image_src( get_field('logo_branco', 'clientes_' . $clientes[0]->term_id), 'full' );
									$cliente_logo = $cliente_logo[0];
								}

								?>
								<h2>Case de sucesso</h2>
								<a href="<?php echo get_permalink( $case->ID ); ?>" class="pic-wrapper">
									<span class="pic">
										<span class="overlay">
											<h3>Qual era a necessidade?</h3>
											<p><?php echo $texto_cartola; ?></p>
										</span>

										<img src="<?php echo $imagem_cartola[0]; ?>" alt="">
									</span>

									<?php
									if($cliente_logo) {
										?>
										<span class="caption caption-logo">
											<span class="caption-inner">
												<span class="logo-cliente valign">
													<img src="<?php echo $cliente_logo; ?>" alt="" class="center"><i></i>
												</span>
												<i class="icon icon-arrow-right"></i>
											</span>
										</span>
										<?php
									}
									?>
								</a>
								<?php
							}
							?>
						</div>

						<div class="col-lg-1"><br></div>

						<div class="col-lg-5 col-right">
							<h2>Conheça também</h2>

							<?php
							if( is_array( $solucoes_lista ) && count( $solucoes_lista ) > 0 ) {
								foreach ($solucoes_lista as $item) {
									$chamada_conheca_item = get_field('chamada_conheca', $item->ID);
									$imagem = wp_get_attachment_image_src( get_field( 'imagem' , $item->ID ), 'solucoes_solucoes' );

									?>
									<a href="<?php echo get_permalink( $item->ID ); ?>" class="pic-wrapper">
										<span class="pic">
											<img src="<?php echo $imagem[0]; ?>" alt="">
										</span>

										<span class="caption">
											<span class="caption-inner">
												<h3><?php echo get_the_title( $item->ID ); ?></h3>
												<p><?php echo $chamada_conheca_item; ?></p>
												<i class="icon icon-arrow-right"></i>
											</span>
										</span>
									</a>
									<?php
								}
							}
							?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</main>
	<?php
}
?>

<?php get_footer(); ?>