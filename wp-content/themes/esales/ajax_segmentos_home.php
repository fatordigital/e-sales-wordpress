<?php
define('WP_USE_THEMES', false);
require_once('../../../wp-load.php');

$slug_segmento = $_GET['segmento'];

if( $slug_segmento ) {
	$segmento_tax = get_term_by( 'slug', $slug_segmento, 'segmentos' );

	$WP_solucao_filtro = array(
			'post_type' => 'combo_home',
			'showposts' => 1,
			'orderby'   => 'date',
			'order'     => 'DESC',
			'tax_query' => array(
				array(
					'taxonomy' => 'segmentos',
					'field'    => 'term_id',
					'terms'    => $segmento_tax->term_id
				)
			)
		);

	$WP_solucao = new WP_Query($WP_solucao_filtro);

	if ( $WP_solucao->have_posts() ) {
		while ( $WP_solucao->have_posts() ) {
			$WP_solucao->the_post();

			$texto_introducao = get_field('texto_introducao');
			$produtos_relacionados = get_field('produtos_relacionados');
			$case_relacionado = get_field('case_relacionado');
			$texto_solucao = get_field('texto_solucao');
			$solucao_relacionada = get_field('solucao_relacionada');

			$imagem_fundo = wp_get_attachment_image_src( get_field('imagem_fundo'), 'full' );
			$logo_cliente = wp_get_attachment_image_src( get_field('logo_cliente'), 'full' );
			$logo_solucao = wp_get_attachment_image_src( get_field('logo_solucao'), 'full' );

			?>

			<div class="bg-esq"><div class="tip"></div></div>
			<div class="bg-dir" style="background-image: url(<?php echo $imagem_fundo[0]; ?>);"></div>

			<div class="container">
				<div class="row content">
					<div class="col-lg-6 col-txt">
						<p><?php echo $texto_introducao; ?></p>

						<?php
						if( is_array( $produtos_relacionados ) && count( $produtos_relacionados ) > 0 ) {
							?>
							<ul>
								<?php
								foreach ($produtos_relacionados as $carac) {
									?>
									<li><span><a href="<?php echo get_permalink($carac); ?>"><?php echo get_the_title($carac); ?></a></span></li>
									<?php
								}
								?>
							</ul>
							<?php
						}
						?>
					</div>

					<div class="col-lg-6 col-cta valign">
						<div class="center">
							<?php
							if( is_array($case_relacionado) && count($case_relacionado) > 0 ) {
								?>
								<a href="<?php echo get_permalink( $case_relacionado[0] ); ?>">
									<div class="veja-solucao">
										<p>Veja a solução utilizada para</p>
										<img src="<?php echo $logo_cliente[0]; ?>">
									</div>
								</a>
								<?php
							}
							?>

							<?php
							if( is_array( $solucao_relacionada ) && count( $solucao_relacionada ) > 0 ) {
								?>
								<div class="link-detalhe">
									<p><?php echo $texto_solucao; ?></p>
									<a href="<?php echo get_permalink( $solucao_relacionada[0] ); ?>" class="bt-cta">
										<img src="<?php echo $logo_solucao[0]; ?>">
									</a>
								</div>
								<?php
							}
							?>
						</div><i></i>
					</div>
				</div>
			</div>
			<?php

		}
	}

	wp_reset_postdata();
}