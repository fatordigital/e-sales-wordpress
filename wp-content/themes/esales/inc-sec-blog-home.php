<section class="sec-blog-home">
  <div class="container">
    <div class="col-left valign">
      <div class="center">
        <h2>Blog da e-Sales</h2>
      </div><i></i>
    </div>

    <div class="col-right lista-posts">

      <?php
      $WP_blog_filtro = array(
          'post_type' => 'blog',
          'showposts' => 3,
          'orderby'   => 'date',
          'order'     => 'DESC'
        );

      $WP_blog = new WP_Query($WP_blog_filtro);

      if ( $WP_blog->have_posts() ) {
        while ( $WP_blog->have_posts() ) {
          $WP_blog->the_post();

          ?>
          <a href="<?php the_permalink(); ?>" class="post valign">
            <span class="center"><?php the_title(); ?></span><i></i>

            <span class="caption">
              <span class="caption-inner">
                <span><?php the_excerpt(); ?></span>
                <i class="icon icon-arrow-right"></i>
              </span>
            </span>
          </a>
          <?php

        }
      }

      wp_reset_postdata();
      ?>

    </div>
  </div>
</section>