/* globals jQuery */
(function($) {
  'use strict';

  $(function() {
    $('.bt-menu').on('click', function(e) {
      e.preventDefault();
      $('body').toggleClass('menu-open');
    });

    $('select').selectric();

    $('.floatlabel').floatlabel();
    $('.placefloater').focus(function() {
      $(this).next('.placefllabel').addClass('placefllabelshow');
    });
    $('textarea.placefloater').change(function() {
      if ($.trim($(this).val()).length < 1) {
        $(this).next('.placefllabel').removeClass('placefllabelshow');
      } else {
         $(this).next('.placefllabel').addClass('placefllabelshow');
      }
    });


    $('.vitrine').slick({
      arrows: false,
      dots: true,
      appendDots: '.vitrine-wrapper .dots',
      slide: '.vitrine-item',
      slidesToShow: 1,
      slidesToScroll: 1,
      autoplay: true,
      autoplaySpeed: 5000,
      pauseOnHover: false,
      speed: 1000,
      cssEase: 'cubic-bezier(0.645, 0.045, 0.355, 1)'
    });

    $('.depoimentos').slick({
      arrows: false,
      dots: true,
      appendDots: '.depoimentos-wrapper .dots',
      slide: '.depoimentos-item',
      slidesToShow: 1,
      slidesToScroll: 1,
      speed: 1000,
      cssEase: 'cubic-bezier(0.645, 0.045, 0.355, 1)'
    });

    var DrawSvg = (function() {
      var defaults = {
        duration: 1000,
        easing: 'easeOutQuart',
        reverse: false,
        stagger: 100,
        callback: $.noop
      };

      var fn = function fn($svg, options) {
        var _this = this;

        _this.options = $.extend(defaults, options);

        _this.$paths = $svg.find('path');

        _this.$paths.each(function(i, elm) {
          var pathElm = elm,
            pathLength = pathElm.getTotalLength();

          pathElm.pathLen = pathLength;
          pathElm.style.strokeDasharray = [pathLength, pathLength].join(' ');
          pathElm.style.strokeDashoffset = pathLength;
        });
      };

      fn.prototype.animate = function animate() {
        var _this = this;

        _this.$paths.each(function(i, elm) {
          setTimeout(function() {
            $({
              pathLen: 0
            }).animate({
              pathLen: elm.pathLen
            }, {
              easing: _this.options.easing,
              duration: _this.options.duration,
              step: function(now, fx) {
                elm.style.strokeDashoffset =
                  (_this.options.reverse ? (fx.end - now) : (now - fx.end)) + 'px';
              },
              complete: function() {
                elm.style.strokeDashoffset = 0;
                _this.options.callback.call(elm);
              }
            });
          }, i * _this.options.stagger);
        });
      };

      return fn;
    })();

    var $tabWrapper = $('#tab-wrapper'),
        $tabContent = $('.tab-content'),
        $solucaoContent = $('#solucao-content'),
        $connectorPath = $('#connector-path').find('path'),
        $navTabs = $('.nav-tabs'),
        $tabPane = $('.tab-pane'),
        // points = [1.5, 240.5, 480.5, 718.5, 957.5],
        // points = [],
        connectorHeight = $('#connector-path').height(),
        names = {
          'industria': [
          	['Fornecedores', 'fornecedores'],
          	['Clientes', 'clientes'],
          	['Governo', 'governo'],
          	['Transportadoras', 'transportadoras'],
          	['Bancos', 'bancos']
          ],
          'atacado': [
          	['Fornecedores', 'fornecedores'],
          	['Clientes', 'clientes'],
          	['Governo', 'governo'],
          	['Transportadoras', 'transportadoras'],
          	['Bancos', 'bancos']
          ],
          'varejo': [
          	['Fornecedores', 'fornecedores'],
          	['Clientes', 'clientes'],
          	['Governo', 'governo'],
          	['Transportadoras', 'transportadoras'],
          	['Bancos', 'bancos']
          ],
          'transportador': [
          	['Fornecedores', 'fornecedores'],
          	['Embarcador', 'embarcador'],
          	['Governo', 'governo'],
          	['Operador Logístico', 'operador-logistico'],
          	['Bancos', 'bancos']
          ],
          'governo': [
          	['Secretarias', 'secretarias'],
          	['Tribunais', 'tribunais'],
          	['Municípios', 'municipios'],
          	['Estados', 'estados'],
          	['Bancos', 'bancos']
          ]
        };

    $navTabs.find('.solucao-item').each(function(i, elm) {
      var $this = $(this),
          d = new DrawSvg($this.find('svg'));

      setTimeout(function() {
        setTimeout(function() {
          d.animate();
        }, 300);

        $this.addClass('come-in');
      }, i * 200);
    });

    $solucaoContent.hide();

    $navTabs.on('click', '.solucao-item', function(e) {
      e.preventDefault();

      var $this = $(this),
          tabName = $this.data('tab-name');

      $tabPane.find('.item').removeClass('show');

      if (!$this.hasClass('ativo')) {

        $tabContent.addClass('show');
        $('.solucao-default').addClass('hide');

        $this.siblings('.solucao-item.ativo').removeClass('ativo');
        $this.addClass('ativo');
        $tabWrapper.removeClass().addClass('tab-ativa-' + tabName);
        $tabPane.find('.item.ativo').removeClass('ativo');
        $connectorPath.attr('d', '');
        $solucaoContent.removeClass('done').delay(100).stop(true).slideUp(1000, 'easeInOutExpo');

        $('body,html').animate({
          scrollTop: $navTabs.offset().top - 10
        });

        $tabContent.find('h3').text(function(i) {
          return names[tabName][i][0];
        });

        $tabContent.find('a').each(function(i){
        	$(this).data('item-name', names[tabName][i][1]);
        });

        var $lineHorz = $tabContent.find('.line-horz').prop('className', 'line-horz'),
            $linesVertBottom = $('.connectors').find('.line-vert-bottom').find('.line').removeClass('show');

        var idx = $this.index('.solucao-item');

        $('.connectors').find('.line-vert-top').find('.line').removeClass('show').eq(idx).addClass('show');

        setTimeout(function() {
          $lineHorz.addClass('animate-in line-' + (idx + 1));

          $tabPane.find('.item').each(function(i) {
            var $this = $(this);

            setTimeout(function() {
              $this.addClass('show');
              $linesVertBottom.eq(i).addClass('show');
            }, i * 100);
          });
        }, 600);

      } else {

        $tabContent.removeClass('show');
        $('.solucao-default').removeClass('hide');

        $this.removeClass('ativo');
        $tabWrapper.removeClass();
        $connectorPath.attr('d', '');
        $solucaoContent.removeClass('done').delay(100).stop(true).slideUp(1000, 'easeInOutExpo');

        $tabContent.find('.line-horz').prop('className', 'line-horz');
        $tabPane.find('.item').removeClass('show');

      }
    });

    $tabPane.on('click', '.item a', function(e) {
      e.preventDefault();

      var $item = $(this).closest('.item'),
        $solucaoAtivo = $navTabs.find('.solucao-item.ativo'),
        target = $(this).data('item-name') + '-' + $solucaoAtivo.data('tab-name');

      if (!$item.hasClass('ativo')) {
        $item.siblings('.item.ativo').removeClass('ativo');

        $solucaoContent.removeClass('done').delay(100).stop(true).slideUp(1000, 'easeInOutExpo', function() {
          //// target
          var targetURL = '_ajax_content.php';
          if ($('#segmentos-home').length) {
            targetURL = $('#segmentos-home').data('url');
          }

          $.get(targetURL, {
            segmento: target
          }).done(function(data) {
            $solucaoContent.html(data);
            $solucaoContent.stop(true).slideDown(1000, 'easeInOutExpo', function() {
              $(this).addClass('done');
            });
          });
        });

        var a = $solucaoAtivo.index('.solucao-item'),
            b = $item.index('.item'),
            itemWidth = $solucaoAtivo.width();

        // $connectorPath.attr('d', 'M ' + points[a] + ' 0 V 102.5 H ' + points[b] + ' V 195');
        // $connectorPath.attr('d', 'M ' + ( (a*itemWidth) + (itemWidth/2) ) + ' 0 V 102.5 H ' + ( (b*itemWidth) + (itemWidth/2) ) + ' V 195');
        $connectorPath.attr('d', 'M ' + ( (a*itemWidth) + (itemWidth/2) ) + ' 0 V '+ (connectorHeight/2) +' H ' + ( (b*itemWidth) + (itemWidth/2) ) + ' V '+connectorHeight);

        var p = new DrawSvg($('#connector-path'), {
          easing: 'easeInOutQuad',
          duration: 350,
          reverse: true,
          callback: function() {
            $item.addClass('ativo');
          }
        });

        p.animate();
      }
    });


    /*======================================
      Clientes
    ======================================*/
    var $listaClientesWrapper = $('.lista-clientes-wrapper'),
        $listaClientes = $listaClientesWrapper.find('.lista-clientes'),
        $clienteCaption = $('#cliente-caption');

    $listaClientes.on('mouseenter', '.logo-cliente[data-link]', function(e) {
      e.preventDefault();

      var $this = $(this),
          offLeft = $listaClientes.offset().left,
          posLeft = $this.offset().left;

      $this.siblings('.logo-cliente').removeClass('hover');
      $this.addClass('hover');

      $clienteCaption.find('h2').text($this.data('title'));
      $clienteCaption.find('p').text($this.data('content'));
      $clienteCaption.find('a').prop('href', $this.data('link'));

      $clienteCaption.css('left', function() {
        var pos = posLeft - offLeft;

        if ( posLeft + $clienteCaption.outerWidth() >= offLeft + $listaClientesWrapper.width() ) {
          pos = pos - $clienteCaption.outerWidth() + $this.outerWidth();
        }

        return pos;
      });

      $clienteCaption.addClass('show');
    });

    $listaClientesWrapper.on('mouseleave', function() {
      $clienteCaption.removeClass('show');
      $listaClientes.find('.logo-cliente.hover').removeClass('hover');
    });

    $listaClientes.slick({
      speed: 200,
      slide: '.logo-cliente',
      slidesToShow: 3,
      slidesToScroll: 1,
      prevArrow: '<button type="button" class="slick-prev"><div class="icon icon-triangle-left"></div></button>',
      nextArrow: '<button type="button" class="slick-next"><div class="icon icon-triangle-right"></div></button>',
      responsive: [{
        breakpoint: 992,
        settings: {
          slidesToShow: 1
        }
      }]
    }).on('beforeChange', function() {
      $clienteCaption.removeClass('show');
      $(this).find('.logo-cliente.hover').removeClass('hover');
    });


    /*======================================
      Cases Detalhe
    ======================================*/
    var $slideWrapper = $('.slide-wrapper'),
      $pag = $('.slide-controls').find('.pag');

    $slideWrapper.on('init', function(e, slick) {
      $pag.find('.curr').text(slick.currentSlide + 1);
      $pag.find('.total').text(slick.slideCount);
    });

    $slideWrapper.on('beforeChange', function(e, slick, currentSlide, nextSlide) {
      $pag.find('.curr').text(nextSlide + 1);
    });

    $slideWrapper.slick({
      slide: '.slide-item',
      slidesToShow: 1,
      slidesToScroll: 1,
      speed: 1000,
      cssEase: 'cubic-bezier(0.645, 0.045, 0.355, 1)',
      prevArrow: '.slide-controls .bt-prev',
      nextArrow: '.slide-controls .bt-next'
    });   

    /*======================================
      Form Contato
    ======================================*/
    $.addAjaxResponse('form_padrao', function(json) {
      if (json.status === 1) {
        if(json.minha_url!= undefined){
          ga('send','pageview',json.minha_url);
        }
        $(this).removeClass('isSending').addClass('isComplete');
        $(this).hide();
        $('#agradecimento-contato').fadeIn();        
      } else {
        $(form).removeClass('isSending').addClass('hasErrors');
      }
    });

    $.addAjaxResponse('form_produtos', function(json){
      if (json.status === 1) {
        $(this).removeClass('isSending').addClass('isComplete');
        $(this).hide();
        $('#agradecimento-contato').fadeIn();
      } else {
        $(form).removeClass('isSending').addClass('hasErrors');
      }
    });

    $('#form_contato').formProc({
      onStart: function(form) {
        //$(form).removeClass('hasErrors isComplete isSending');
        $(form).find('.field-error').removeClass('field-error');
        $(form).find('.form-error-message').hide();
      },
      onError: function(input, form) {
        $(input).closest('.field').addClass('field-error');
        //$(input).focus();
        console.log(form);
        $(form).find('.form-error-message').show();
      },
      onEnd: function(form, valid) {
        if (valid) {
          $(form).addClass('isSending');
          var myBtn = $(form).find('button');
          $(myBtn).text('Enviando ...'); 
          $(myBtn).addClass('myOpacity'); 
          $(myBtn).prop("disabled", true);   
        } else {
          $(form).find('.form-error-message').show();
        }
      },
      stopOnError: false
    });

    $('#form-produtos').formProc({
      onStart: function(form) {
        //$(form).removeClass('hasErrors isComplete isSending');
        $(form).find('.field-error').removeClass('field-error');
        $(form).find('.form-error-message').hide();
      },
      onError: function(input, form) {
        $(input).closest('.field').addClass('field-error');
        //$(input).focus();
        $(form).find('.form-error-message').fadeIn();
      },
      onEnd: function(form, valid) {
        if (valid) {
          $(form).addClass('isSending');
          $(form).hide();
          $('#agradecimento-contato2').fadeIn(300);
        } else {
          $(form).find('.form-error-message').fadeIn();
        }
      },
      stopOnError: false
    });

    $('#form-news').formProc({
      onStart: function(form) {
        //$(form).removeClass('hasErrors isComplete isSending');
        $(form).find('.field-error').removeClass('field-error');
        //$(form).find('.form-error-message').hide();
      },
      onError: function(input, form) {
        $(input).closest('.field').addClass('field-error');
        //$(input).focus();
        //$(form).find('.form-error-message').fadeIn();
      },
      onEnd: function(form, valid) {
      	console.log('NEWS', form, valid);
        if (valid) {
          $(form).hide();
          $('#news-obg').fadeIn(300);

        } else {
          //$(form).find('.form-error-message').fadeIn();
        }
      },
      stopOnError: false
    });

    $('#form_sol').formProc({
      onStart: function(form) {
        //$(form).removeClass('hasErrors isComplete isSending');
        $(form).find('.field-error').removeClass('field-error');
        $(form).find('.form-error-message').hide();
      },
      onError: function(input, form) {
        $(input).closest('.field').addClass('field-error');
        //$(input).focus();
        console.log(form);
        $(form).find('.form-error-message').show();
      },
      onEnd: function(form, valid) {
        if (valid) {
          $(form).addClass('isSending');
          var myBtn = $(form).find('button');
          $(myBtn).text('Enviando ...'); 
          $(myBtn).addClass('myOpacity');
          $(myBtn).prop("disabled", true);         
        } else {
          $(form).find('.form-error-message').show();
        }
      },
      stopOnError: false
    });

    $('#form_p').formProc({
      onStart: function(form) {
        //$(form).removeClass('hasErrors isComplete isSending');
        $(form).find('.field-error').removeClass('field-error');
        $(form).find('.form-error-message').hide();
      },
      onError: function(input, form) {
        $(input).closest('.field').addClass('field-error');
        //$(input).focus();
        console.log(form);
        $(form).find('.form-error-message').show();
      },
      onEnd: function(form, valid) {
        if (valid) {
          $(form).addClass('isSending');
          var myBtn = $(form).find('button');
          $(myBtn).text('Enviando ...'); 
          $(myBtn).addClass('myOpacity');
          $(myBtn).prop("disabled", true);   
        } else {
          $(form).find('.form-error-message').show();
        }
      },
      stopOnError: false
    });


    /*======================================
      Área Cliente
    ======================================*/
    $('.aviso-manu').hide();
    //$('div').hide();
    $('.manutencao').bind('mouseover', function() {
      $('.aviso-manu').stop(true).hide();
      $('.aviso-manu').html($(this).find('a').find('p').html());
      $('.aviso-manu').fadeIn();
    });
    $('.manutencao').bind('mouseout', function() {
      $('.aviso-manu').stop(true).fadeOut();
    });

    /*======================================
		Compartilhamentos
    ======================================*/
		$('.social a').on('click', function(){
			var url = $(this).attr('href');
			window.open(url,'pop','width=450, height=450, top=100, left=100, scrollbars=no');
			return false;
		});

  });
}(jQuery));
