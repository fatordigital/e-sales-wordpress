<?php
/*
 * Template Name: Soluções
 */
?>
<?php get_header(); ?>

<?php

while( have_posts() ) {
  the_post();

  $img_cartola = wp_get_attachment_image_src( get_field('imagem_cartola'), 'full' );

  $topicos = get_field('topicos'); //titulo, texto

  $cases = get_field('cases');

  $share_links = get_share_links();
}

?>

<main>
  <div class="hero">
    <div class="overlay">
      <div class="container valign">
        <div class="center">
          <h2>Soluções</h2>
        </div><i></i>
      </div>
    </div>

    <div class="bg" style="background-image: url(<?php echo $img_cartola[0]; ?>);"></div>
  </div>

  <div class="main-content">
    <div class="bg-esq"></div>
    <div class="bg-dir"></div>
    <div class="bg-center">
      <div class="bg-center-inner">
        <svg class="img" viewBox="0 0 1920 657" style="background-color:#ffffff00" version="1.1"
          xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xml:space="preserve"
          x="0px" y="0px" width="1920px" height="657px">
          <path class="shape-bg" d="M 0 0 L 523 57 L 960 2.7641 L 1397 57 L 1920 3.191 L 1920 657 L 0 657 L 0 0 Z" fill="#ffffff"/>
        </svg>
      </div>
    </div>

    <div class="sec-solucoes-lista">
      <div class="container">

        <div class="row solucoes-lista-intro">
          <?php
          if( is_array( $topicos ) && count( $topicos ) > 0 ) {
            foreach ($topicos as $item) {
              ?>
              <div class="col-lg-6">
                <h2><?php echo $item['titulo']; ?></h2>
                <p><?php echo $item['texto']; ?></p>
              </div>
              <?php
            }
          }
          ?>
        </div>

        <div class="row">
          <div class="col-lg-12">
            <h2>Conheça todas as soluções</h2>

            <div class="lista-solucoes">
              <a href="/solucoes/documentos-fiscais-eletronicos/" target="_blank" class="item ico-sol-documentos">
                <span class="item-inner"><h3>DOCUMENTOS FISCAIS ELETRÔNICOS</h3></span>
              </a>
              <a href="/solucoes/integracao-de-dados/" target="_blank" class="item ico-sol-dados">
                <span class="item-inner"><h3>INTEGRAÇÃO DE DADOS</h3></span>
              </a>
              <a href="/solucoes/integracao-bancaria/" target="_blank" class="item ico-sol-integracao">
                <span class="item-inner"><h3>INTEGRAÇÃO BANCÁRIA</h3></span>
              </a>
              <a href="/solucoes/supply-chain-mercantil/" target="_blank" class="item ico-sol-supply">
                <span class="item-inner"><h3>SUPPLY CHAIN MERCANTIL</h3></span>
              </a>
              <a href="/solucoes/logistica-da-distribuicao/" target="_blank" class="item ico-sol-logistica">
                <span class="item-inner"><h3>LOGÍSTICA DA DISTRIBUIÇÃO</h3></span>
              </a>
            </div>
            <?php /*
            <div class="lista-solucoes">

              <?php
              $WP_solucoes_filtro = array(
                  'post_type' => 'solucoes',
                  'showposts' => -1,
                  'orderby'   => 'date',
                  'order'     => 'DESC'
                );

              $WP_solucoes = new WP_Query($WP_solucoes_filtro);

              if ( $WP_solucoes->have_posts() ) {
                while ( $WP_solucoes->have_posts() ) {
                  $WP_solucoes->the_post();
                  ?>
                  <a href="<?php the_permalink(); ?>" class="item" >
                    <span class="item-inner">
                      <h3><?php the_title(); ?></h3>
                      <i class="icon icon-arrow-right"></i>
                    </span>
                  </a>
                  <?php

                }
              }

              wp_reset_postdata();
              ?>
            </div>
            */ ?>
          </div>
        </div>
      </div>
    </div>

    <div class="clear"></div>

    <?php get_template_part('inc-sec-contato'); ?>

    <div class="sec-lista-cases">
      <div class="container">
        <div class="row">
          <div class="col-lg-12">
            <h2>Veja outros cases de sucesso</h2>
          </div>
        </div>

        <div class="row">
          <?php
          if( is_array( $cases ) && count( $cases ) ) {
            foreach ($cases as $case) {
              $texto_cartola = get_field('texto_cartola', $case->ID);
              $imagem_cartola = wp_get_attachment_image_src( get_field('imagem_cartola', $case->ID), 'solucoes_case');

              $clientes = wp_get_post_terms( $case->ID, 'clientes' );
              $cliente_logo = '';
              if( is_array( $clientes ) && count( $clientes ) > 0 ) {
                $cliente_logo = wp_get_attachment_image_src( get_field('logo_branco', 'clientes_' . $clientes[0]->term_id), 'full' );
                $cliente_logo = $cliente_logo[0];
              }
              ?>
              <a href="<?php echo get_permalink( $case->ID ); ?>" class="col-lg-6 pic-wrapper">
                <div class="pic">
                  <img src="<?php echo $imagem_cartola[0]; ?>" alt="">
                  <div class="overlay">
                    <h3>Qual era a necessidade?</h3>
                    <p><?php echo $texto_cartola; ?></p>
                  </div>
                </div>

                <?php
                if($cliente_logo) {
                  ?>
                  <span class="caption caption-logo">
                    <span class="caption-inner">
                      <span class="logo-cliente valign">
                        <img src="<?php echo $cliente_logo; ?>" alt="" class="center"><i></i>
                      </span>
                      <i class="icon icon-arrow-right"></i>
                    </span>
                  </span>
                  <?php
                }
                ?>
              </a>
              <?php
            }
          }
          ?>

        </div>
      </div>
    </div>
  </div>
</main>

<?php get_footer(); ?>