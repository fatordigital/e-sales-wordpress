<?php get_header(); ?>

<main>
  <div class="hero">
    <div class="overlay">
      <div class="container valign">
        <div class="center">
          <!-- <h2>BLOG e-Sales</h2> -->
        </div><i></i>
      </div>
    </div>
    <div class="bg" style="background-image: url(<?php bloginfo( 'template_url' ); ?>/img/hero-sec-blog.jpg);"></div>
  </div>

  <div class="main-content">
    <div class="bg-esq"></div>
    <div class="bg-dir"></div>
    <div class="bg-center">
      <div class="bg-center-inner">
        <svg class="img" viewBox="0 0 1920 657" style="background-color:#ffffff00" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xml:space="preserve" x="0px" y="0px" width="1920px" height="657px">
          <path class="shape-bg" d="M 0 0 L 523 57 L 960 2.7641 L 1397 57 L 1920 3.191 L 1920 657 L 0 657 L 0 0 Z" fill="#ffffff" />
        </svg>
      </div>
    </div>

    <div class="sec-blog">
      <div class="container">
        <div class="row">
 					<div class="col-lg-9">
						<?php
						while( have_posts() ) {
							the_post();

							$imagem_destaque = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'large' );

							$share_links = get_share_links();

							$post_categories = wp_get_post_categories( $post->ID );
							$cats = array();

							foreach($post_categories as $c){
								$cat = get_category( $c );
								$cats[] = array( 'name' => $cat->name, 'slug' => $cat->slug );
							}

							?>

		            <div class="post-item cf">

		              <?php
		              if( count( $cats ) > 0 ) {
		              	?>
										<div class="post-cat cf"><span></span>
			                <h4><?php echo $cats[0]->name; ?></h4>
			              </div>
		              	<?php
		              }
		              ?>

		              <h1>
		              	<a href="<?php the_permalink(); ?>">
			              	<?php the_title(); ?>
		              	</a>
	              	</h1>
		              <div class="col-lg-3 post-info">
		                <div class="post-data"><?php echo strtoupper( get_the_time('d/M') ); ?></div>

		                <p class="comp">Compartilhe</p>
		                <div class="social">
		                  <a href="<?php echo $share_links['facebook']; ?>" class="icon icon-facebook" title="Facebook"></a>
		                  <a href="<?php echo $share_links['twitter']; ?>" class="icon icon-twitter" title="Twitter"></a>
		                  <a href="<?php echo $share_links['googleplus']; ?>" class="icon icon-googleplus" title="Google+"></a>
		                  <a href="<?php echo $share_links['linkedin']; ?>" class="icon icon-linkedin" title="LinkedIn"></a>
		                </div>

		              </div>

		              <div class="col-lg-9 post-conteudo">
		              	<?php
		              	if( $imagem_destaque && $imagem_destaque[0] ) {
		              		?>
	              			<a href="<?php the_permalink(); ?>">
												<img src="<?php echo $imagem_destaque[0]; ?>" alt="">
											</a>
		              		<?php
		              	}
		              	?>
		              	<a href="<?php the_permalink(); ?>">
		                	<p><?php the_excerpt(); ?></p>
		                </a>
		              </div>

		            </div>

		          <?php
						}
						?>
					</div>

          <div class="col-lg-3 lat-blog">

            <div class="lat-categorias">
              <h3>Categorias</h3>
              <?php
							$categories = get_categories( $args );
							if( is_array( $categories ) && count( $categories ) > 0 ) {
								foreach ($categories as $cat) {
									?>
									<div class="post-cat cf"><span></span>
		                <h4><a href="<?php echo get_post_type_archive_link( 'blog' ); ?>?cat_name=<?php echo $cat->slug; ?>"><?php echo $cat->name; ?></a></h4>
		              </div>
									<?php
								}
							}
							?>
            </div>

            <div class="lat-curta">
              <h3>Curta E-Sales</h3>
              <div class="fb-page" data-href="https://www.facebook.com/Esalessolucoes?fref=ts" data-width="270" data-height="230" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true" data-show-posts="false">
                <div class="fb-xfbml-parse-ignore">
                  <blockquote cite="https://www.facebook.com/Esalessolucoes?fref=ts"><a href="https://www.facebook.com/Esalessolucoes?fref=ts">E sales Soluções de Integração</a></blockquote>
                </div>
              </div>
            </div>

            <div class="lat-posts">
              <h3>Posts Recentes</h3>
              <?php
							$WP_blog_filtro = array(
									'post_type' => 'blog',
									'showposts' => 3,
									'orderby'   => 'date',
									'order'     => 'DESC'
								);

							$WP_blog = new WP_Query($WP_blog_filtro);

							if ( $WP_blog->have_posts() ) {
								?>
								<ul>
								<?php
								while ( $WP_blog->have_posts() ) {
									$WP_blog->the_post();
									?>
									<li>
										<a href="<?php the_permalink(); ?>">
											<?php the_title(); ?>
										</a>
									</li>
									<?php
								}
								?>
								</ul>
								<?php
							}

							wp_reset_postdata();
							?>
            </div>

          </div>

          <div class="clear"></div>
        </div>
      </div>
    </div>
  </div>
</main>

<?php get_footer(); ?>