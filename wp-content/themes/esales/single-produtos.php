
<?php

// form envio 
  $destinatarios = "marketing@esales.com.br";
  //$destinatarios = "rosalba.monteiro@fatordigital.com.br";
  $agradecimento_titulo = get_field('agradecimento_titulo');
  $agradecimento_texto = get_field('agradecimento_texto');

  

  if( is_post() ) {
    header('Cache-Control: no-cache, must-revalidate');
    header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
    header('Content-type: application/json');

    $post_fc_nome = $_POST['nome'];
    $post_fc_email = $_POST['email'];
    $post_fc_fone = $_POST['telefone'];
    $post_fc_pagina = $_POST['pagina'];
    $post_fc_mensagem = $_POST['mensagem'];

    include('rdstation.php');
    RdStation::addLeadConversionToRdstationCrm($_POST['pagina'], array(
        'email'     => $_POST['email'], 
        'nome'      => $_POST['nome'],
        'produtos'  => $_POST['prd'],
        'telefone'  => $_POST['telefone'],
        'mensagem'  => $_POST['mensagem'],
        )
    );

    //echo "<script>ga('send','pageview','http://".$_SERVER['HTTP_HOST']."/goal".$_SERVER['REQUEST_URI']."');</script>";

    $template = ' <p><strong>Nome: </strong> ' . $post_fc_nome . '</p>
                  <p><strong>Email: </strong> ' . $post_fc_email . '</p>
                  <p><strong>Fone: </strong> ' . $post_fc_fone . '</p>
                  <p><strong>Segmento: </strong> ' . $post_fc_pagina . '</p>
                  <p><strong>Mensagem: </strong> ' . $post_fc_mensagem . '</p>';


    $headers[] = 'Content-type:text/html;';
    $headers[] = 'Reply-to: ' . $post_fc_email;

    $envio = wp_mail( $destinatarios, '[E-Sales] Contato soluções', utf8_decode($template), $headers );


    echo json_encode( array( 'status' => 1, 'mensagem' => 'Envio ok', 'destino' => $destinatarios, 'minha_url' =>"/goal".$_SERVER['REQUEST_URI']) );

    exit();
  }
?>

<?php get_header(); ?>

<?php
while( have_posts() ) {
  the_post();

  //CARTOLA
  $imagem = wp_get_attachment_image_src( get_field('imagem'), 'full' );
  $logo = wp_get_attachment_image_src( get_field('logo'), 'full' );

  //BOX O QUE E
  $titulo_oque = get_field('titulo_oque');
  $texto_oque = get_field('texto_oque');

  //BOX PROBLEMA
  $titulo_problema = get_field('titulo_problema');
  $texto_problema = get_field('texto_problema');

  //BOX BENEFICIOS
  $titulo_beneficios = get_field('titulo_beneficios');
  $itens_beneficios = get_field('itens'); //texto

  //BOX ATRIBUTOS
  $titulo_atributos = get_field('titulo_atributos');
  $itens_atributos = get_field('itens_atributos'); //texto
  $video_atributos = get_field('video_atributos');
  $imagem_atributos = wp_get_attachment_image_src( get_field('imagem_atributos'), 'full' );

  //CASES SOLUCOES
  $case_lista = get_field('case');
  $produtos_lista = get_field('produtos_servicos');
  $solucoes_lista = get_field('solucoes');

  //CONTATO
  $titulo_contato = get_field('titulo_contato');
  $nome_rd_station = get_field('nome_rd_station');
  $codigo_rd_station = get_field('codigo_rd_station');

  //SHARE
  $share_links = get_share_links();

  //OUTRAS UNIDADES
  $unidades = array(
      'filetransfer' => 229,
      'interbancos' =>278,
      'oobj' => 285,
      'entregou' => 273
    );

  unset( $unidades[ $post->post_type ] );
  ?>

  <main>
    <div class="hero">
      <div class="overlay">
        <div class="container valign">
          <div class="center">
            <h2><?php the_title(); ?></h2>
            <div class="logo-solucao">
              <img src="<?php echo $logo[0]; ?>" alt="<?php the_title(); ?>">
            </div>
          </div><i></i>
        </div>
      </div>

      <div class="bg" style="background-image: url(<?php echo $imagem[0]; ?>);"></div>
    </div>

    <div class="main-content">
      <div class="bg-esq"></div>
      <div class="bg-dir"></div>
      <div class="bg-center">
        <div class="bg-center-inner">
          <svg class="img" viewBox="0 0 1920 657" style="background-color:#ffffff00" version="1.1"
            xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xml:space="preserve"
            x="0px" y="0px" width="1920px" height="657px">
            <path class="shape-bg" d="M 0 0 L 523 57 L 960 2.7641 L 1397 57 L 1920 3.191 L 1920 657 L 0 657 L 0 0 Z" fill="#ffffff"/>
          </svg>
        </div>
      </div>

      <div class="sec-scm cf">
        <div class="col-left-full">
          <div class="box-o-que-e cf">
            <div class="content">
              <h2><?php echo $titulo_oque; ?></h2>
              <?php echo $texto_oque; ?>
            </div>
          </div>
        </div>

        <div class="col-right-full">
          <div class="col-right-full-inner">
            <h3><?php echo $titulo_problema; ?></h3>
            <?php echo $texto_problema; ?>

            <div class="sep"></div>

            <h3><?php echo $titulo_beneficios; ?></h3>
            <ul>
              <?php
              if( is_array( $itens_beneficios ) && count( $itens_beneficios ) > 0 ) {
                foreach ($itens_beneficios as $item) {
                  ?>
                  <li><span><?php echo $item['texto']; ?></span></li>
                  <?php
                }
              }
              ?>
            </ul>
          </div>
        </div>
      </div>

      <div class="clear"></div>

      <div class="sec-atributos cf">
        <div class="bg-esq"></div>

        <div class="container">
          <div class="row">
            <div class="col-lg-5">
              <h2><?php echo $titulo_atributos; ?></h2>
              <ul>
                <?php
                if( is_array( $itens_atributos ) && count( $itens_atributos ) > 0 ) {
                  foreach ($itens_atributos as $item) {
                    ?>
                    <li><span><?php echo $item['texto']; ?></span></li>
                    <?php
                  }
                }
                ?>
              </ul>
            </div>

            <div class="col-lg-7">
              <?php
              if( $video_atributos ) {
                $embed_code = wp_oembed_get( $video_atributos, array( 'width' => 600, 'height' => 460 ) );

                if( $embed_code ) {
                  echo $embed_code;
                }
              } elseif( is_array( $imagem_atributos ) && $imagem_atributos[0] ) {
                ?>
                <img src="<?php echo $imagem_atributos[0]; ?>" alt="">
                <?php
              }

              ?>
            </div>
          </div>
        </div>
      </div>
      <!--- formulario -->
        <div class="sec-form">
          <div class="container">
            <div class="row">
              <h2>Entre em contato sobre essa solução</h2>              
              <form action="<?php the_permalink(); ?>" method="post" data-ajax="form_padrao" id="form_p">
                <div class="div-metade right">
                  <div class="form-group field">
                    <input type="text" class="form-control" placeholder="Nome" name="nome" data-req="required">
                  </div>
                  <div class="form-group field">
                    <input type="email" class="form-control" placeholder="E-mail" name="email" data-req="required">
                  </div>
                  <div class="form-group field">
                    <input type="tel" class="form-control" placeholder="Telefone" name="telefone" data-req="required" data-mask="(99) 99999-9999">
                  </div>
                  <div class="form-group ">
                    <input type="hidden" class="form-control" value="<?php the_slug(); ?>" name="pagina" >
                    <input type="hidden" class="form-control" value="<?php the_title(); ?>" name="prd" >
                  </div>
                </div>
                <div class="div-metade left">
                  <div class="form-group field">
                    <textarea class="form-control" rows="7" placeholder="Mensagem" name="mensagem" data-req="required"></textarea>
                  </div>
                  <div class="form-group">
                    <button class="btn btn-white" data-href="Enviar" >Enviar</button>
                  </div>
                </div>
              </form>
              <div id="agradecimento-contato" style="display:none;">
                  <div class="box-agradecimento">
                    <h1>Obrigado por entrar em contato!</h1>
                    <p>Agradecemos seu interesse em nossa empresa, em breve nossa equipe fará contato.<br>
                        Aproveite para conhecer nossos demais cases e produtos navegando pelo site.<br>
                        Compartilhe sua opinião conosco. Nos acompanhe nas redes sociais e em nosso blog.</p>
                  </div>
                  <a href="<?php echo home_url(); ?>" class="bt-padrao">Ir para página inicial</a>
              </div>
            </div>
          </div>
        </div>
        <!--- formulario -->

      <div class="sec-case cf">
        <div class="bg-dir"></div>

        <div class="container">
          <div class="row">
            <?php
            if( is_array( $case_lista ) && count( $case_lista ) > 0 ) {
              $case = $case_lista[0];

              $texto_cartola = get_field('texto_cartola', $case->ID);
              $imagem_cartola = wp_get_attachment_image_src( get_field('imagem_cartola', $case->ID), 'full');

              $clientes = wp_get_post_terms( $case->ID, 'clientes' );
              $cliente_logo = '';
              if( is_array( $clientes ) && count( $clientes ) > 0 ) {
                $cliente_logo = wp_get_attachment_image_src( get_field('logo_branco', 'clientes_' . $clientes[0]->term_id), 'full' );
                $cliente_logo = $cliente_logo[0];
              }

              ?>

              <a href="<?php echo get_permalink( $case->ID ); ?>" class="col-lg-12 pic-wrapper">
                <div class="pic">
                  <div class="foto-case" style="background-image: url(<?php echo $imagem_cartola[0]; ?>);" ></div>
                  <div class="overlay">
                    <h2>Case de sucesso</h2>

                    <h3>Qual era a necessidade?</h3>
                    <p><?php echo $texto_cartola; ?></p>
                  </div>
                </div>
                <?php
                if($cliente_logo) {
                  ?>
                  <span class="caption caption-logo">
                    <span class="caption-inner">
                      <span class="logo-cliente valign">
                        <img src="<?php echo $cliente_logo; ?>" alt="" class="center"><i></i>
                      </span>
                      <i class="icon icon-arrow-right"></i>
                    </span>
                  </span>
                  <?php
                }
                ?>
              </a>
              <?php
            }
            ?>
          </div>
        </div>
      </div>

      <div class="sec-servicos-complementares cf">
        <div class="container">
          <div class="row">
            <div class="col-lg-7 center-block aligncenter">
              <h2>Produtos e serviços complementares</h2>
            </div>

            <div class="lista-servicos-complementares">
              <?php
              if( is_array( $produtos_lista ) && count( $produtos_lista ) > 0 ) {
                foreach ($produtos_lista as $produto) {
                  ?>
                  <a href="<?php echo get_permalink( $produto->ID ); ?>" class="item col-lg-4">
                    <span class="item-inner">
                      <h3><?php echo get_the_title( $produto->ID ); ?></h3>
                      <i class="icon icon-arrow-right"></i>
                    </span>
                  </a>
                  <?php
                }
              }
              ?>
            </div>
          </div>
        </div>
      </div>

      <?php get_template_part('inc-sec-contato'); ?>

      <div class="sec-outro sec-outro-single">
        <div class="container">
          <div class="row aligncenter">
            <div class="col-lg-12">
              <h2>Conheça também</h2>
            </div>
          </div>

          <div class="row">
            <div class="col-right cf">

              <?php
              foreach ($unidades as $key_pt => $page_id) {
                $imagem = wp_get_attachment_image_src( get_field( 'imagem_cartola' , $page_id ), 'produtos_solucoes' );
                ?>
                <div class="col-lg-4">
                  <a href="<?php echo get_permalink( $page_id ); ?>" class="pic-wrapper">
                    <span class="pic">
                      <img src="<?php echo $imagem[0]; ?>" alt="">
                    </span>

                    <span class="caption">
                      <span class="caption-inner">
                        <h3><?php echo get_the_title( $page_id ); ?></h3>
                        <i class="icon icon-arrow-right"></i>
                      </span>
                    </span>
                  </a>
                </div>
                <?php
              }
              /*if( is_array( $solucoes_lista ) && count( $solucoes_lista ) > 0 ) {
                foreach ($solucoes_lista as $solucao) {
                  $imagem = wp_get_attachment_image_src( get_field( 'imagem' , $item->ID ), 'produtos_solucoes' );
                  ?>
                  <div class="col-lg-4">
                    <a href="<?php echo get_permalink( $solucao->ID ); ?>" class="pic-wrapper">
                      <span class="pic">
                        <img src="<?php echo $imagem[0]; ?>" alt="">
                      </span>

                      <span class="caption">
                        <span class="caption-inner">
                          <h3><?php echo get_the_title( $solucao->ID ); ?></h3>
                          <i class="icon icon-arrow-right"></i>
                        </span>
                      </span>
                    </a>
                  </div>

                  <?php
                }
              }*/
              ?>

            </div>
          </div>
        </div>
      </div>
    </div>
  </main>

  <?php
}
?>

<?php get_footer(); ?>