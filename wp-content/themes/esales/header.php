<!DOCTYPE html>
<html lang="pt-br">
<head>
  <meta charset="utf-8">
  <!--
  /**
   *
   * @copyright 2015 by Aldeia
   * http://www.aldeia.biz
   *
   **/
  --><!--[if IE]><![endif]-->
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <title><?php wp_title( '|', true, 'right' ); bloginfo( 'name' ); ?></title>
  <link rel="stylesheet" href="<?php bloginfo( 'template_url' ); ?>/style.css">
  <link rel="shortcut icon" href="<?php bloginfo( 'template_url' ); ?>/favicon.ico">
  <meta name="application-name" content="<?php bloginfo( 'name' ); ?>">
  <meta name="msapplication-TileColor" content="#FFFFFF">
  <meta name="msapplication-TileImage" content="<?php bloginfo( 'template_url' ); ?>/windows-tile.png">
  <!--  Apple/IOS  -->
  <link rel="apple-touch-icon" href="<?php bloginfo( 'template_url' ); ?>/apple-touch-icon.png">
  <meta name="description" content="<?php echo get_bloginfo( 'description' ); ?>">

  <?php
  if( is_page( 273 ) || is_singular('entregou') ) {
  	?><meta name="keywords" content="Logística, SrM, tms, yms, scm, fiscal, order, order tracking, tracking, logística reversa, cálculo de fretes, fretes, gateway de fretes, gateway, edi fiscal, entregou.com, entregou, edipharma, edivarejo, edi varejo, edi farma, edi pharma, edifarma, edi 96ª, edi proceda, edi transportes, edi logístico, edi compras, edi supply chain, cadeia de valor, cadeia de suprimentos, colaboração na cadeia produtiva, b2b, b2b2c, B2B, B2B2C, tracking transportes, mobile entregas, entregas mobile, tracking mobile, tracking correios, fretes correios, gateway correios, e-commerce fretes, e-commerce correios, planejamento e reposição,  visibilidade varejo, visibilidade de estoque, integração entre ERPs,  integrar ERP, centros de distribuição,  Demand Chain, value chain, DRP, SOP, VMI, CPFR, Retail Intelligence, gestão de suprimentos, controle de entregas, agendamento de entrega, agenda de entregas, agenda de compras, MRP, material Planning, B4B, ediesales, edi e-sales, edi esales, planejamento de demanda, tracking transportes, ocorrências de entregas, ocor entrega, notfis edi, desadv edi, packing list edi, invoic edi, logística para e-commerce."><?php
  }

  if( is_page( 278 ) || is_singular('interbancos') ) {
  	?><meta name="keywords" content="Cobrança, pagamentos, extratos, boletos, arrecadação, varredura títulos, consignação, folha de pagto, folha de pagamento, pagamento a fornecedores, títulos de fornecedores, antecipação de títulos, boletos bancários, cnab, cnab bradesco, cnab itau, cnab, santander, cnab banco brasil, cnab bb, cnab caixa, cnab cef, recebíveis, cartões, conciliação bancaria, conciliação cartões, fluxo de caixa, interbancos, intervan, interconexão."><?php
  }

  if( is_page( 229 ) || is_singular('filetransfer') ) {
  	?><meta name="keywords" content="Odette, Http, OFTP, FTP, FTPs, sFTP, file transfer, integration, webservices, connect, connectdirect, colaboração, collaboration,  RVS, EDI, Enterprise Data Integration, @EDI, edi enterprise, recievmailer, sendmailer, localcarrier, edi corporativo, gesthor, Edifact, edi anfavea, edi renavan, edi recall, proceda, business to business, file transfer security, security transfer, e commerce, tracking services, security business, Integração edi, integration solution, electronic data interchange, Edifact, connection integration, connection integrated."><?php
  }

  if( is_page( 285 ) || is_singular('oobj') ) {
  	?><meta name="keywords" content="NF-e, nfe, MD-e, mde, recebe dfe, NFC-e, nfce, mdfe, MDF-e, recebe xml, nota fiscal eletrônica, nota eletrônica, conhecimento eletrônico, documento eletrônico, xml nfe, xml NF-e, xml cte, xml CT-e, danfe, dacte, damfe, e-governo, egoverno, nfe email, emitir nfc-e, manifestação destinatário, manifesto de cargas, validação fiscal, BI fiscal, NFS-e, Nota Fiscal Eletrônica."><?php
  }
  ?>

  <meta name="robots" content="all">
  <?php if( is_home() ) { ?>
  <meta name="viewport" content="initial-scale=1,maximum-scale=1,user-scalable=no">
<?php } ?>

  <meta property="og:title" content="<?php wp_title( '|', true, 'right' ); bloginfo( 'name' ); ?>">
  <meta property="og:type" content="website">
  <meta property="og:site_name" content="<?php bloginfo( 'name' ); ?>">
  <meta property="og:url" content="<?php echo 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']; ?>">
  <!-- Twitter -->
  <meta name="twitter:card" content="summary" />
  <meta name="twitter:site" content="@e_Sales" />
  <meta name="twitter:creator" content="@aldeiabiz" />
  <?php
  if( is_single() || is_singular() || is_page() ) {
  	echo '<!--SINGULAR-->';
  	$current_post = $post;
  	$post = get_queried_object();
  	setup_postdata($post);

  	$img_share = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' );
  	?>
		<meta property="og:description" content="<?php echo get_the_excerpt(); ?>">
		<?php
		if( $img_share && $img_share[0] ) {
			?>
			<meta property="og:image" content="<?php echo $img_share[0]; ?>">
			<meta property="og:image:width" content="<?php echo $img_share[1]; ?>">
			<meta property="og:image:height" content="<?php echo $img_share[2]; ?>">
			<?php
		} else {
			?>
			<meta property="og:image" content="<?php bloginfo( 'template_url' ); ?>/share.png">
			<meta property="og:image:width" content="1200">
			<meta property="og:image:height" content="630">
			<?php
		}?>
  	<?php
  	$post = $current_post;
  	setup_postdata($post);
  } else {
  	?>
		<meta property="og:description" content="<?php echo get_bloginfo( 'description' ); ?>">
		<meta property="og:image" content="<?php bloginfo( 'template_url' ); ?>/share.png">
		<meta property="og:image:width" content="1200">
		<meta property="og:image:height" content="630">
  	<?php
  }
  ?>


  <!--[if lt IE 9]><script src="<?php bloginfo( 'template_url' ); ?>/js/vendor/html5shiv.min.js"></script><![endif]-->
  <?php wp_head(); ?>

  <script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
	ga('create', 'UA-63308146-1', 'auto');
	ga('send', 'pageview');
	</script>

	<!-- Facebook Conversion Code for Pixel - Visualizações Site e-Sales -->
	<script>(function() {var _fbq = window._fbq || (window._fbq = []);
	if (!_fbq.loaded) {var fbds = document.createElement('script');
	fbds.async = true;
	fbds.src = '//connect.facebook.net/en_US/fbds.js';
	var s = document.getElementsByTagName('script')[0];
	s.parentNode.insertBefore(fbds, s);
	_fbq.loaded = true;
	}
	})();
	window._fbq = window._fbq || [];
	window._fbq.push(['track', '6033099358906', {'value':'0.00','currency':'BRL'}]);
	</script>
	<noscript><img height="1" width="1" alt="" style="display:none" src="https://www.facebook.com/tr?ev=6033099358906&amp;cd[value]=0.00&amp;cd[currency]=BRL&amp;noscript=1" /></noscript>
</head>

<?php
$body_class = is_home() ? 'home' : 'interna';
?>

<body class="pt <?php echo $body_class; ?>">
	<!-- Google Tag Manager -->
	<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-N5D95N"
	height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
	new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
	j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
	'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
	})(window,document,'script','dataLayer','GTM-N5D95N');</script>
	<!-- End Google Tag Manager -->

	<div id="fb-root"></div>
	<script>(function(d, s, id) {
	  var js, fjs = d.getElementsByTagName(s)[0];
	  if (d.getElementById(id)) return;
	  js = d.createElement(s); js.id = id;
	  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.5&appId=737153136417532";
	  fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));</script>

	<header>
	  <div class="container">


			<?php if( is_home() ) { ?>
      <a href="<?php echo home_url(); ?>" class="topo-logo"><img src="<?php bloginfo( 'template_url' ); ?>/img/logo.png" alt="E-Sales"></a>
				<a href="<?php echo get_page_link(158); ?>" class="link-area-cliente">Área do Cliente</a>
      <?php } else { ?>
      <a href="<?php echo home_url(); ?>" class="topo-logo"><img src="<?php bloginfo( 'template_url' ); ?>/img/logo-interna.png" alt="E-Sales"></a>
      <?php } ?>

	    <nav>
	      <ul class="main-nav">
	        <li <?php ativo( is_page(36) ); ?>><a href="<?php echo get_page_link(36); ?>" title="e-Sales" class="tit-esales">e-SALES</a></li>
	        <li <?php ativo( is_page(50) || is_singular('solucoes') ); ?>><a href="<?php echo get_page_link(50); ?>" title="Soluções">Soluções</a></li>
	        <li <?php ativo( is_page(38) || is_singular( array('filetransfer', 'interbancos', 'oobj', 'entregou') ) ); ?>><a href="<?php echo get_page_link(38); ?>" title="Produtos">Produtos</a></li>
	        <li <?php ativo( is_page(48) || is_singular('cases') ); ?>><a href="<?php echo get_page_link(48); ?>" title="Cases">Cases</a></li>
	        <li <?php ativo( is_page(12) ); ?>><a href="<?php echo get_page_link(12); ?>" title="Clientes">Clientes</a></li>
	        <li <?php ativo( is_post_type_archive('blog') || is_singular('blog') ); ?>><a href="<?php echo get_post_type_archive_link('blog'); ?>" title="Blog">Blog</a></li>
					<?php if( !is_home() ) { ?>
	        	<li <?php ativo( is_page(158) ); ?>><a href="<?php echo get_page_link(158); ?>" title="Área do Cliente">Área do Cliente</a></li>
	        <?php } ?>
	        <li <?php ativo( is_page(14) ); ?>><a href="<?php echo get_page_link(14); ?>" title="Fale Conosco">Fale Conosco</a></li>
          <li class="link-area-cliente-mob"><a href="<?php echo get_page_link(158); ?>">Área do Cliente</a></li>
	      </ul>

	      <div class="search">
	        <form action="<?php echo get_page_link(40); ?>">
	          <input type="text" name="busca" id="busca" placeholder="Faça sua busca">
	          <button type="submit"><i class="icon icon-arrow-right"></i></button>
	        </form>
	      </div>
	    </nav>
      <button class="bt-menu" title="Menu">
        <span class="hamburguer">
          <span class="bar bar-1"></span>
          <span class="bar bar-2"></span>
          <span class="bar bar-3"></span>
        </span>
      </button>
	  </div>
	</header>