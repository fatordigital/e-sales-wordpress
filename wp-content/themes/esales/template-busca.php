<?php
/*
 * Template Name: Busca
 */
?>
<?php get_header(); ?>

<?php
$get_busca = $_GET['busca'];


//SOLUCOES
$WP_solucoes_filtro = array(
		'post_type' => 'solucoes',
		'showposts' => -1,
		'orderby'   => 'date',
		'order'     => 'DESC',
		's' 				=> $get_busca
	);

$WP_solucoes = new WP_Query($WP_solucoes_filtro);





//PRODUTOS
$WP_produtos_filtro = array(
		'post_type' => array( 'filetransfer', 'interbancos', 'oobj', 'entregou' ),
		'showposts' => -1,
		'orderby'   => 'date',
		'order'     => 'DESC',
		's' 				=> $get_busca
	);

$WP_produtos = new WP_Query($WP_produtos_filtro);

if ( $WP_produtos->have_posts() ) {
	while ( $WP_produtos->have_posts() ) {
		$WP_produtos->the_post();

	}
}

wp_reset_postdata();


//BLOG
$WP_blog_filtro = array(
		'post_type' => 'blog',
		'showposts' => -1,
		'orderby'   => 'date',
		'order'     => 'DESC',
		's' 				=> $get_busca
	);

$WP_blog = new WP_Query($WP_blog_filtro);

if ( $WP_blog->have_posts() ) {
	while ( $WP_blog->have_posts() ) {
		$WP_blog->the_post();

	}
}

wp_reset_postdata();

?>

<main class="sec-resultados">
  <div class="hero">
    <div class="overlay">
      <div class="container valign">
        <div class="center">
          <h2>Foram encontrados <?php echo ( $WP_solucoes->found_posts + $WP_produtos->found_posts + $WP_blog->found_posts ); ?> resultados para "<?php echo $get_busca; ?>"</h2>
        </div><i></i>
      </div>
    </div>
    <div class="bg" style="background:#113A72"></div>
  </div>
    <div class="main-content">
      <div class="bg-esq"></div>
      <div class="bg-dir"></div>
      <div class="bg-center">
        <div class="bg-center-inner">
          <svg class="img" viewBox="0 0 1920 657" style="background-color:#ffffff00" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xml:space="preserve" x="0px" y="0px" width="1920px" height="657px">
            <path class="shape-bg" d="M 0 0 L 523 57 L 960 2.7641 L 1397 57 L 1920 3.191 L 1920 657 L 0 657 L 0 0 Z" fill="#ffffff" />
          </svg>
        </div>
      </div>
      <div class="container">
        <div class="row">
				<?php
				if ( $WP_solucoes->have_posts() ) {
					?>
					<div class="lista-resultados cf">
            <div class="contador-resultados">
              <h2>Soluções</h2>
              <h3>Foram encontrados <?php echo $WP_solucoes->found_posts; ?> resultados.</h3>
            </div>
						<div class="box-solucao cf">
							<?php
							while ( $WP_solucoes->have_posts() ) {
								$WP_solucoes->the_post();
								$imagem_destaque = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'solucoes_solucoes' );
								?>
									<a href="<?php the_permalink(); ?>" class="pic-wrapper">
									<span class="pic">
										<img src="<?php echo $imagem_destaque[0]; ?>" alt="">
										</span>
										<span class="caption">
	                    <span class="caption-inner">
		                    <h3><?php the_title(); ?></h3>
		                    <i class="icon icon-arrow-right"></i>
	                    </span>
                    </span>
									</a>
								<?php
							}
							?>
						</div>
					</div>
					<?php
				}
				wp_reset_postdata();

				if ( $WP_produtos->have_posts() ) {
					?>
					<div class="lista-resultados cf">
						<div class="contador-resultados">
              <h2>Produtos</h2>
              <h3>Foram encontrados <?php echo $WP_produtos->found_posts; ?> resultados.</h3>
            </div>
						<div class="box-solucao cf">
							<?php
							while ( $WP_produtos->have_posts() ) {
								$WP_produtos->the_post();
								$imagem_destaque = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'solucoes_solucoes' );

								?>
									<a href="<?php the_permalink(); ?>" class="pic-wrapper">
									<span class="pic">
										<img src="<?php echo $imagem_destaque[0]; ?>" alt="">
										</span>
										<span class="caption">
	                    <span class="caption-inner">
		                    <h3><?php the_title(); ?></h3>
		                    <i class="icon icon-arrow-right"></i>
	                    </span>
                    </span>
									</a>
								<?php
							}
							?>
						</div>
					</div>
					<?php
				}

				wp_reset_postdata();



				if ( $WP_blog->have_posts() ) {
					?>
					<div class="lista-resultados cf">
						<div class="contador-resultados">
              <h2>Demais resultados</h2>
              <h3>Foram encontrados <?php echo $WP_blog->found_posts; ?> resultados.</h3>
            </div>
						<div class="demais-resultados">
						<h3>Blog</h3>
						<?php
							while ( $WP_blog->have_posts() ) {
								$WP_blog->the_post();
								$imagem_destaque = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'solucoes_solucoes' );

								?>
								<div class="item">
									<a href="<?php the_permalink(); ?>">
										<h4><?php the_title(); ?></h4>
										<p><?php echo get_the_excerpt(); ?></p>
									</a>
								</div>
								<?php
							}
							?>
						</div>
					</div>
					<?php
				}
				wp_reset_postdata();
				?>
			</div>
    </div>
  </div>
</main>



<?php get_footer(); ?>