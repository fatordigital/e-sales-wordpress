<?php get_header(); ?>

<?php
while( have_posts() ) {
	the_post();

	//CARTOLA
	$titulo_cartola = get_field('titulo_cartola');
	$texto_cartola = get_field('texto_cartola');
	$imagem_cartola =  wp_get_attachment_image_src( get_field('imagem_cartola'), 'full' );

	//CONTEUDO
	$titulo_conteudo = get_field('titulo_conteudo');
	$texto_conteudo = get_field('texto_conteudo');
	$produtos_lista = get_field('produtos');

	//DEPOIMENTO
	$depoimento_lista = get_field('depoimento');
	$depoimento_video = get_field('video_depoimento');
	$imagem_depoimento = wp_get_attachment_image_src( get_field('imagem_depoimento'), 'full' );

	//DIFERENCIAIS
	$titulo_diferenciais = get_field('titulo_diferenciais');
	$itens_diferenciais = get_field('itens_diferenciais'); //titulo, texto, imagem

	//GALERIA
	$galeria = get_field('galeria'); //imagem

	//FUNCIONALIDADES
	$funcionalidades = get_field('funcionalidades'); //texto

	//SERVIÇOS
	$servicos = get_field('servicos'); //texto

	//CASES
	$titulo_cases = get_field('titulo_outros_cases');
	$cases_lista = get_field('cases_outros_cases');

	//CLIENTE
	$clientes = wp_get_post_terms( $post->ID, 'clientes' );
	$cliente_logo = '';
	if( is_array( $clientes ) && count( $clientes ) > 0 ) {
		$cliente_logo = wp_get_attachment_image_src( get_field('logo_branco', 'clientes_' . $clientes[0]->term_id), 'full' );
		$cliente_logo = $cliente_logo[0];
	}

	//SHARE
	$share_links = get_share_links();

	?>
	<main>
		<div class="hero hero-case">
			<div class="overlay">
				<div class="container valign">
					<div class="center">
						<h3><?php echo $titulo_cartola; ?></h3>

						<div class="row">
							<div class="col-lg-7">
								<p><?php echo $texto_cartola; ?></p>
							</div>
							<div class="col-lg-5">
								<?php
								if( $cliente_logo ) {
									?>
									<div class="logo-cliente">
										<img src="<?php echo $cliente_logo; ?>" alt="">
									</div>
									<?php
								}
								?>

							</div>
						</div>
					</div><i></i>
				</div>
			</div>

			<div class="bg" style="background-image: url(<?php echo $imagem_cartola[0]; ?>);"></div>
		</div>

		<div class="main-content">
			<div class="bg-esq"></div>
			<div class="bg-dir"></div>
			<div class="bg-center">
				<div class="bg-center-inner">
					<svg class="img" viewBox="0 0 1920 657" style="background-color:#ffffff00" version="1.1"
						xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xml:space="preserve"
						x="0px" y="0px" width="1920px" height="657px">
						<path class="shape-bg" d="M 0 0 L 523 57 L 960 2.7641 L 1397 57 L 1920 3.191 L 1920 657 L 0 657 L 0 0 Z" fill="#ffffff"/>
					</svg>
				</div>
			</div>

			<div class="sec-detalhe-case">
				<div class="container">
					<div class="intro">
						<h1><?php echo $titulo_conteudo; ?></h1>
					</div>
				</div>

				<div class="col-left-full">
					<div class="col-left-full-inner">
						<?php echo $texto_conteudo; ?>
					</div>
				</div>

				<div class="col-right-full">
					<div class="col-right-full-inner">
						<?php
						if( is_array( $produtos_lista ) && count( $produtos_lista ) > 0 ) {
							?>
							<h2>Produtos</h2>
							<ul>
								<?php
								foreach ($produtos_lista as $item) {
									?>
									<li><a href="<?php echo get_permalink( $item->ID ); ?>"><span><?php echo get_the_title( $item->ID ); ?></span></a></li>
									<?php
								}
								?>
							</ul>
							<?php
						}
						?>
					</div>
				</div>
			</div>

			<div class="clear"></div>

			<?php
			//$depoimento_lista
			//$depoimento_video
			//$imagem_depoimento

			if( is_array( $depoimento_lista ) && count( $depoimento_lista ) > 0 ) {
				$current_post = $post;

				$post = $depoimento_lista[0];
				setup_postdata( $post );

				$autor = get_field('autor', $post->ID);
				$funcao = get_field('funcao', $post->ID);

				//$depoimento_imagem = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'cases_depoimentos' );

				$clientes_dep = wp_get_post_terms( $post->ID, 'clientes' );
				$cliente_logo_dep = '';
				if( is_array( $clientes_dep ) && count( $clientes_dep ) > 0 ) {
					$cliente_logo_dep = wp_get_attachment_image_src( get_field('logo_branco', 'clientes_' . $clientes_dep[0]->term_id), 'full' );
					$cliente_logo_dep = $cliente_logo_dep[0];
				}
				?>
				<div class="sec-depoimento">
					<div class="bg-esq"></div>

					<div class="container">
						<div class="row">
							<div class="col-lg-5">
								<h2>Depoimento</h2>
								<div class="quote">
									<?php the_content(); ?>
								</div>
								<div class="quote-author">
									<?php
									if( $cliente_logo_dep ) {
										?>
										<div class="logo-cliente">
											<img src="<?php echo $cliente_logo_dep; ?>" alt="">
										</div>
										<?php
									}
									?>
									<h3><?php echo $autor; ?></h3>
									<p><?php echo $funcao; ?></p>
								</div>
							</div>

							<div class="col-lg-7 col-right">
								<?php
								$show_imagem = true;
								if( $depoimento_video ) {
									$embed_video = wp_oembed_get( $depoimento_video, array( 'width' => 610, 'height' => 460 ) );
									if( $embed_video ) {
										$show_imagem = false;
										echo $embed_video;
									}
								}

								if( $show_imagem && ( is_array($imagem_depoimento) && $imagem_depoimento[0] ) ) {
									?>
									<img src="<?php echo $imagem_depoimento[0]; ?>" alt="">
									<?php
								}
								?>
							</div>
						</div>
					</div>
				</div>
				<?php

				$post = $current_post;
				setup_postdata( $post );
			}

			//$titulo_diferenciais
			//$itens_diferenciais //titulo, texto, imagem
			?>

			<div class="sec-diferenciais">
				<div class="bg-dir"></div>

				<div class="container">
					<div class="sec-diferenciais-inner">
						<h2><?php echo $titulo_diferenciais; ?></h2>

						<ul>
							<?php
							if( is_array( $itens_diferenciais ) && count( $itens_diferenciais ) > 0 ) {
								foreach ($itens_diferenciais as $dif_item) {
									$dif_imagem = wp_get_attachment_image_src( $dif_item['imagem'], 'full' );
									?>
									<li>
										<h3><?php echo $dif_item['titulo']; ?></h3>
										<p><?php echo $dif_item['texto']; ?></p>
										<div class="icon-wrapper">
											<img src="<?php echo $dif_imagem[0]; ?>" alt="">
										</div>
									</li>
									<?php
								}
							}
							?>
						</ul>

						<div class="barra-icones"></div>
					</div>
				</div>
			</div>

			<div class="sec-galeria">
				<div class="container">
					<?php
					if( is_array($galeria) && count($galeria) > 0 ) {
						?>
						<div class="slide-wrapper">
							<?php
							foreach ($galeria as $gal) {
								$imagem = wp_get_attachment_image_src( $gal['imagem'], 'full' );
								?>
								<div class="slide-item">
									<img src="<?php echo $imagem[0]; ?>" alt="">
								</div>
								<?php
							}
							?>
						</div>
						<?php
					}
					?>

					<div class="row">
						<div class="col-lg-8 col-info">
							<div class="col-info-inner">
								<?php
								if( is_array($funcionalidades) && count($funcionalidades) > 0 ) {
									?>
									<h2>Funcionalidades</h2>
									<ul>
										<?php
										foreach ($funcionalidades as $func) {
											?>
											<li><span><?php echo $func['texto']; ?></span></li>
											<?php
										}
										?>
									</ul>
									<?php
								}
								?>

								<?php
								if( is_array($servicos) && count($servicos) > 0 ) {
									?>
									<h2>Serviços</h2>
									<ul>
										<?php
										foreach ($servicos as $func) {
											?>
											<li><span><?php echo $func['texto']; ?></span></li>
											<?php
										}
										?>
									</ul>
									<?php
								}
								?>
							</div>
						</div>

						<div class="col-controls">
							<?php
							if( is_array($galeria) && count($galeria) > 1 ) {
								?>
								<div class="slide-controls">
									<div class="slide-controls-inner">
										<div class="row">
											<button class="bt-prev"><i class="icon icon-chevron-left"></i></button>
											<button class="bt-next"><i class="icon icon-chevron-right"></i></button>
											<div class="pag"><span class="curr">1</span>/<span class="total"><?php echo count( $galeria ); ?></span></div>
										</div>
									</div>
								</div>
								<?php
							}
							?>

							<div class="compartilhe-case">
								<p>Compartilhe este case</p>

								<div class="social">
									<a href="<?php echo $share_links['facebook']; ?>" target="_blank" class="icon icon-facebook" title="Facebook"></a>
									<a href="<?php echo $share_links['twitter']; ?>" target="_blank" class="icon icon-twitter" title="Twitter"></a>
									<a href="<?php echo $share_links['linkedin']; ?>" target="_blank" class="icon icon-linkedin" title="LinkedIn"></a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="sec-lista-cases">
				<div class="container">
					<div class="row">
						<div class="col-lg-12">
							<h2><?php echo $titulo_cases; ?></h2>
						</div>
					</div>

					<div class="row">

						<?php
						if( is_array( $cases_lista ) && count( $cases_lista ) > 0 ) {
							foreach ($cases_lista as $case_item) {
								$case_texto_cartola = get_field('texto_cartola', $case_item->ID);
								$case_imagem_cartola = wp_get_attachment_image_src( get_field('imagem_cartola', $case_item->ID), 'solucoes_case');

								$case_clientes = wp_get_post_terms( $case_item->ID, 'clientes' );
								$case_cliente_logo = '';
								if( is_array( $case_clientes ) && count( $case_clientes ) > 0 ) {
									$case_cliente_logo = wp_get_attachment_image_src( get_field('logo_branco', 'clientes_' . $case_clientes[0]->term_id), 'full' );
									$case_cliente_logo = $case_cliente_logo[0];
								}

								?>
								<a href="<?php echo get_permalink($case_item->ID); ?>" class="col-lg-6 pic-wrapper">
									<div class="pic">
										<img src="<?php echo $case_imagem_cartola[0]; ?>" alt="">
										<div class="overlay">
											<h3>Qual era a necessidade?</h3>
											<p><?php echo $case_texto_cartola; ?></p>
										</div>
									</div>
									<?php
									if($case_cliente_logo) {
										?>
										<span class="caption">
											<span class="caption-inner">
												<span class="logo-cliente valign">
													<img src="<?php echo $case_cliente_logo; ?>" alt="" class="center"><i></i>
												</span>
												<i class="icon icon-arrow-right"></i>
											</span>
										</span>
										<?php
									}
									?>
								</a>
								<?php
							}
						}
						?>
					</div>
				</div>
			</div>

			<div class="clear"></div>
		</div>
	</main>
	<?php
}
?>

<?php get_footer(); ?>