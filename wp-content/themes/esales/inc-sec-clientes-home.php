<section class="sec-clientes-home">
  <div class="bg-left"></div>
  <div class="bg-right"></div>

  <div class="container">
    <div class="lista-clientes-wrapper">
      <div class="caption" id="cliente-caption">
        <div class="caption-inner">
          <div class="col-left">
            <h2></h2>
            <p></p>
          </div>

          <div class="col-right">
            <a href="#" class="bt-cta">Veja o case</a>
          </div>
        </div>
      </div>

      <div class="lista-clientes">

        <?php
        $WP_cases_filtro = array(
        		'post_type' => 'cases',
        		'showposts' => -1,
        		'orderby'   => 'date',
        		'order'     => 'DESC'
        	);

        $WP_cases = new WP_Query($WP_cases_filtro);

        if ( $WP_cases->have_posts() ) {
        	while ( $WP_cases->have_posts() ) {
        		$WP_cases->the_post();

        		//$texto_cartola = get_field('texto_cartola', $post->ID);
						//$imagem_cartola = wp_get_attachment_image_src( get_field('imagem_cartola', $post->ID), 'solucoes_case');

						$clientes = wp_get_post_terms( $post->ID, 'clientes' );
						$cliente_logo = '';
						if( is_array( $clientes ) && count( $clientes ) > 0 ) {
							$cliente_logo = wp_get_attachment_image_src( get_field('logo_branco', 'clientes_' . $clientes[0]->term_id), 'full' );
							$cliente_logo = $cliente_logo[0];
						}
            ?>
            <div class="logo-cliente valign" data-link="<?php echo the_permalink(); ?>" data-title="Qual era a necessidade?" data-content="<?php echo get_field( 'texto_cartola' ); ?>">

              <div class="center">
                <img src="<?php echo $cliente_logo; ?>" alt="">
              </div><i></i>
              <div class="icon icon-star"></div>
            </div>
            <?php

          }
        }

        wp_reset_postdata();
        /*$clientes = get_clientes();

        //print_r( $clientes );

        if( is_array($clientes) && count($clientes) > 0 ) {
          foreach ($clientes as $cliente) {
            //T.term_id, T.name, T.slug, TT.taxonomy, TT.description, count_cases

            $case_link = '';
            $logo_listagem = wp_get_attachment_image_src( get_field('logo_listagem', 'clientes_' . $cliente->term_id), 'full');

            if( $cliente->count_cases > 0 ) {
              $WP_case_filtro = array(
                  'post_type' => 'cases',
                  'showposts' => 1,
                  'orderby'   => 'date',
                  'order'     => 'DESC',
                  'tax_query' => array(
                    array(
                      'taxonomy' => 'clientes',
                      'field'    => 'term_id',
                      'terms'    => $cliente->term_id
                    )
                  )
                );

              $WP_case = new WP_Query($WP_case_filtro);

              if ( $WP_case->have_posts() ) {
                while ( $WP_case->have_posts() ) {
                  $WP_case->the_post();

                  $case_link = get_permalink();
                }
              }

              wp_reset_postdata();
            }

            if( $cliente->count_cases > 0 ) {
              ?>
              <div class="logo-cliente valign" data-link="<?php echo $case_link; ?>" data-title="<?php echo $cliente->name; ?>" data-content="<?php echo $cliente->description; ?>">
              <?php
            } else {
              ?>
              <div class="logo-cliente valign">
              <?php
            }?>
              <div class="center">
                <img src="<?php echo $logo_listagem[0]; ?>" alt="">
              </div><i></i>
              <?php
              if( $cliente->count_cases > 0 ) {
                ?>
                <div class="icon icon-star"></div>
                <?php
              }
              ?>
            </div>
            <?php
          }
        }*/
        ?>
      </div>
    </div>

    <div class="col-title">
      <h2>Cases</h2>
    </div>
  </div>
</section>