<?php
/*
 * Template Name: Área do Cliente
 */
?>
<?php get_header(); ?>

<main>

<?php
while( have_posts() ) {
	the_post();
	$img = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' );
	?>
  <div class="hero">
    <div class="overlay">
      <div class="container valign">
        <div class="center">
          <h2><?php the_title(); ?></h2>
        </div><i></i>
      </div>
    </div>

    <div class="bg" style="background-image: url(<?php echo $img[0]; ?>);"></div>
  </div>

  <div class="main-content">
    <div class="bg-esq"></div>
    <div class="bg-dir"></div>

    <div class="bg-center">
      <div class="bg-center-inner">
        <svg class="img" viewBox="0 0 1920 657" style="background-color:#ffffff00" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xml:space="preserve" x="0px" y="0px" width="1920px" height="657px">
          <path class="shape-bg" d="M 0 0 L 523 57 L 960 2.7641 L 1397 57 L 1920 3.191 L 1920 657 L 0 657 L 0 0 Z" fill="#ffffff" />
        </svg>
      </div>
    </div>

    <div class="sec-area-cliente">
      <div class="container">

        <div class="row">
          <div class="col-lg-10 center-block intro">
            <h1>ACESSE</h1>
            <?php the_content(); ?>
          </div>
        </div>

        <div class="lista-area-cliente">
          <ul>
          	<?php
          	$servicos = get_field('servicos');

          	if( is_array( $servicos ) && count( $servicos ) > 0 ) {

          		foreach ($servicos as $item) {
          			$logo_src = wp_get_attachment_image_src( $item['logo'], 'full' );
          			?>
								<li <?php echo $item['bt_manutencao'] ? 'class="manutencao"' : ''; ?>>
		              <a href="<?php echo $item['link']; ?>" target="_blank">
		                <img src="<?php echo $logo_src[0]; ?>" alt="">
		                <span class="bt-padrao-pq">Acessar</span>
		                <p class="hidden">
		                	<?php echo $item['txt_manutencao']; ?>
			              </p>
		              </a>
		              <span class="manu-atencao"></span>
		            </li>
          			<?php
          		}

          	}
          	?>
          </ul>

          <p class="aviso-manu" id="manu-3333-mostra"></p>

        </div>
      </div>
    </div>

  </div>

<?php
}
?>

  <?php get_template_part('inc-sec-clientes-home'); ?>

	<?php get_template_part('inc-sec-blog-home'); ?>

</main>

<?php get_footer(); ?>