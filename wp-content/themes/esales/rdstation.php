<?php 

/**
 * Classe RdStation
 *
 */
 
/* Espaço para comentários, TODOs e explicações das modificações em novas versões desta classe


*/

define('rdstation_token', '0c29a0fc488e0e5827165c65e2b6006f');
define('rdstation_private_token', 'a46d6680683f41287f0653fdac28a1ab');

class RdStation
{

/**
 * RD Station - Integrações
 * addLeadConversionToRdstationCrm()
 * Envio de dados para a API de leads do RD Station
 *
 * Parâmetros:
 *     ($rdstation_token) - token da sua conta RD Station ( encontrado em https://www.rdstation.com.br/docs/api )
 *     ($identifier) - identificador da página ou evento ( por exemplo, 'pagina-contato' )
 *     ($data_array) - um Array com campos do formulário ( por exemplo, array('email' => 'teste@rdstation.com.br', 'nome' =>'Fulano') )
 */
	public static function addLeadConversionToRdstationCrm( $identifier, $data_array ) {
	  $api_url = "http://www.rdstation.com.br/api/1.2/conversions";

	  $rdstation_token = rdstation_token;

	  try {
		if (empty($data_array["token_rdstation"]) && !empty($rdstation_token)) { $data_array["token_rdstation"] = $rdstation_token; }
		if (empty($data_array["identificador"]) && !empty($identifier)) { $data_array["identificador"] = $identifier; }
		if (empty($data_array["c_utmz"])) { $data_array["c_utmz"] = $_COOKIE["__utmz"]; }
		unset($data_array["password"], $data_array["password_confirmation"], $data_array["senha"], 
			  $data_array["confirme_senha"], $data_array["captcha"], $data_array["_wpcf7"], 
			  $data_array["_wpcf7_version"], $data_array["_wpcf7_unit_tag"], $data_array["_wpnonce"], 
			  $data_array["_wpcf7_is_ajax_call"]);

		if ( !empty($data_array["token_rdstation"]) && !( empty($data_array["email"]) && empty($data_array["email_lead"]) ) ) {
      	  $data_query = http_build_query($data_array);
		  if (in_array ('curl', get_loaded_extensions())) {
			$ch = curl_init($api_url);
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $data_query);
			curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			curl_exec($ch);
			curl_close($ch);
		  } else {
			$params = array('http' => array('method' => 'POST', 'content' => $data_query, 'ignore_errors' => true));
			$ctx = stream_context_create($params); 
			$fp = @fopen($api_url, 'rb', false, $ctx);
		  }
		}
	  } catch (Exception $e) { }
	}

/**
 * RD Station - Integrações
 * addWonLostConversionToRdstationCrm()
 * Envio de dados para a API de vendas do RD Station
 *
 * Parâmetros:
 *     ($status) - tipo de conversão (lost or won)
 *     ($data_array) - um Array com campos do formulário ( por exemplo, array('email' => 'teste@rdstation.com.br', 'nome' =>'Fulano') )
 */
	public static function addWonLostConversionToRdstationCrm( $status, $data_array ) {
	  $api_url = "https://www.rdstation.com.br/api/1.2/services/".rdstation_private_token."/generic";

	  $data_array['status'] = $status;

	  try {
		if (!( empty($data_array["email"])) ) {
      	  $data_query = json_encode($data_array);

      	  if (in_array ('curl', get_loaded_extensions())) {
			$ch = curl_init($api_url);
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $data_query);
			curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
			    'Content-Type: application/json',                                                                                
			    'Content-Length: ' . strlen($data_query))                                                                       
			); 
			curl_exec($ch);
			curl_close($ch);
		  } else {
			$params = array('http' => array('method' => 'POST', 'content' => $data_query, 'ignore_errors' => true));
			$ctx = stream_context_create($params); 
			$fp = @fopen($api_url, 'rb', false, $ctx);
		  }
		}
	  } catch (Exception $e) { }
	}

} // end class

