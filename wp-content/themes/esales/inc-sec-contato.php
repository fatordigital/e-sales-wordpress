<div class="sec-contato">
  <div class="container">
    <form action="<?php bloginfo( 'template_url' ); ?>/form_produtos.php" method="post" data-ajax="form_produtos" id="form-produtos">
      <h2>Inscreva-se agora e receba conteúdos gratuitos de negócios</h2>

      <div class="field-group">
        <div class="field">
          <input type="text" name="nome" id="nome" placeholder="Nome" data-req="required">
        </div>

        <div class="field">
          <div class="field-append-button">
            <input type="email" name="email" id="email" placeholder="E-mail" data-req="email">
            <button type="submit"><i class="icon icon-arrow-right"></i></button>
          </div>
        </div>
      </div>

      <div class="form-error-message">Por favor, preencha todos os campos.</div>
    </form>

    <div id="agradecimento-contato2">
      <div class="box-agradecimento">
        <h1>Você foi inscrito com sucesso!</h1>
      </div>
      <?php /* ?><a href="/" class="bt-padrao">Ir para página inicial</a><?php */ ?>
    </div>
  </div>

  <div class="compartilhe-solucao">
    <p>Compartilhe esta solução</p>

    <?php $share_links = get_share_links(); ?>

    <div class="social">
      <a href="<?php echo $share_links['facebook']; ?>" class="icon icon-facebook" title="Facebook"></a>
      <a href="<?php echo $share_links['twitter']; ?>" class="icon icon-twitter" title="Twitter"></a>
      <a href="<?php echo $share_links['linkedin']; ?>" class="icon icon-linkedin" title="LinkedIn"></a>
    </div>
  </div>
</div>