<?php

$texto_rodape = get_field('texto_rodape', 'options');
$formulario_news_rd_station = get_field('formulario_news_rd_station', 'options');
$codigo_news_rd_station = get_field('codigo_news_rd_station', 'options');
$lojas_lista = get_field('lojas', 'options'); //nome, endereco, bairro, cep, fone, email
?>
<footer>
  <div class="container">
    <div class="cols-wrapper">
      <div class="col-lg-3 col-md-3 col-sm-4 logo-anos">
        <img src="<?php bloginfo( 'template_url' ); ?>/img/logo-20-anos.png" alt="">
      </div>
      <div class="col-lg-6 col-md-6 col-sm-8">
        <iframe src="<?php bloginfo( 'template_url' ); ?>/frame-news.php" frameborder="0" id="newsframe" name="newsframe" style="width:0; height:0;"></iframe>

        <h2>Newsletter</h2>

        <div id="news-obg" style="display:none;">
          <h1>Obrigado</h1>
          <p>Seus dados foram enviados com sucesso!</p>
        </div>
        <form id="form-news" method="post" name="form1" action="http://cache.mail2easy.com.br/integracao" target="newsframe">
          <input type="hidden" name="CON_ID" value="26941.21372f10b968491e5997e7f86d0f3825">
          <input type="hidden" name="DESTINO" value="http://www.esales.com.br/news-cadastro/">
          <input type="hidden" name="GRUPOS_CADASTRAR" value="12">
          <input type="hidden" name="GRUPOS_DESCADASTRAR" value="">
          <input type="hidden" name="SMT_RECEBER" value="1">
          <div class="fieldset">
            <h3 class="legend">Receba nossas novidades</h3>
            <div class="field">
              <input type="text" value="" name="SMT_nome" placeholder="Nome" class="floatlabel" data-req="required">
            </div>
            <div class="field field-append-button">
              <input type="text" value="" name="SMT_email" placeholder="E-mail" class="floatlabel" data-req="email">
              <button type="submit" name="submit"><i class="icon icon-arrow-right"></i></button>
            </div>
          </div>
        </form>

      </div>

      <div class="col-lg-3 col-md-3 col-sm-12 fr">
        <h3>Siga-nos nas redes</h3>

        <div class="social">
          <a href="https://www.facebook.com/Esalessolucoes" target="_blank" class="icon icon-facebook" title="Facebook"></a>
          <a href="https://twitter.com/e_Sales" target="_blank" class="icon icon-twitter" title="Twitter"></a>
          <a href="https://plus.google.com/u/2/100732617397322307236" target="_blank" class="icon icon-googleplus" title="Google+"></a>
          <a href="https://www.linkedin.com/company/e-sales-solu-es-de-integra-o" class="icon icon-linkedin" target="_blank" title="LinkedIn"></a>
        </div>
      </div>
    </div>

    <div class="lista-unidades">
      <div class="row">
        <?php
        if( is_array( $lojas_lista ) && count( $lojas_lista ) > 0 ) {
          foreach ($lojas_lista as $loja) {
            ?>
            <div class="col-lg-3 col-md-6 col-sm-12">
              <h3><?php echo $loja['nome'] ?></h3>
              <p class="addr mb10"><?php echo $loja['endereco'] ?><br>
              <?php echo $loja['bairro']; ?> - CEP <?php echo $loja['cep']; ?></p>
              <?php
              if( $loja['fone'] ) {
                ?>
                <p class="tel"><i class="icon icon-tel"></i> <?php echo $loja['fone']; ?></p>
                <?php
              }
              ?>
              <?php
              if( $loja['email'] ) {
                ?>
                <p class="email"><i class="icon icon-mail"></i> <a href="mailto:<?php echo $loja['email']; ?>"><?php echo $loja['email']; ?></a></p>
                <?php
              }
              ?>
            </div>
            <?php
          }
        }
        ?>
      </div>

      <div class="row">
        <div class="col-lg-3 col-md-6 col-sm-12">
          <h3>Unidades Regionais</h3>
          <p class="mb10">Birigui/SP</p>
          <p class="mb10">Novo Hamburgo/RS</p>
          <p class="mb10">Vitória/ES</p>
        </div>

        <div class="col-lg-7 col-sm-12 col-contatos">
          <h3>Contatos</h3>

          <div class="lista-contatos">
            <div class="col">
              <p class="tel"><i class="icon icon-tel"></i> Suporte: (51) 3325.8100</p>
              <p class="email"><i class="icon icon-mail"></i><a href="mailto:rh@esales.com.br" title="Recursos Humanos">rh@esales.com.br</a></p>
              <p class="email"><i class="icon icon-mail"></i><a href="mailto:comercial@esales.com.br" title="Comercial">comercial@esales.com.br</a></p>
            </div>

            <div class="col">
              <p class="email"><i class="icon icon-mail"></i><a href="mailto:marketing@esales.com.br" title="Marketing">marketing@esales.com.br</a></p>
              <p class="email"><i class="icon icon-mail"></i><a href="mailto:financeiro@esales.com.br" title="Financeiro">financeiro@esales.com.br</a></p>
              <p class="email"><i class="icon icon-mail"></i><a href="mailto:diretoria@esales.com.br" title="Diretoria">diretoria@esales.com.br</a></p>
            </div>
          </div>
        </div>


      </div>
    </div>
  </div>

  <div class="barra-logos-solucoes">
    <div class="container">
      <a href="<?php echo get_page_link(273); ?>" class="logo-solucao">
        <img src="<?php bloginfo( 'template_url' ); ?>/img/logo-entregoucom.png" alt="">
      </a>

      <a href="<?php echo get_page_link(285); ?>" class="logo-solucao">
        <img src="<?php bloginfo( 'template_url' ); ?>/img/logo-oobj.png" alt="">
      </a>

      <a href="<?php echo get_page_link(278); ?>" class="logo-solucao">
        <img src="<?php bloginfo( 'template_url' ); ?>/img/logo-interbancos.png" alt="">
      </a>

      <a href="<?php echo get_page_link(229); ?>" class="logo-solucao">
        <img src="<?php bloginfo( 'template_url' ); ?>/img/logo-edienterprise.png" alt="">
      </a>
    </div>
  </div>

  <div class="container copy">
    <div class="row">
      <div class="col-lg-11 col-md-12 col-sm-12 col cc">
        <img src="<?php bloginfo( 'template_url' ); ?>/img/logo-footer.png" alt="" class="logo-footer">
        <p>E-Sales Soluções de Integração Todos os direitos autorais reservados <span class="sep">&bull;</span> <a href="<?php echo get_permalink( get_page_by_path( 'politica-de-privacidade' ) ); ?>"><strong>Política de Privacidade</strong></a></p>
      </div>

      <div class="col-lg-1 col-md-12 col-sm-12 col alignright">
        <p>Por <a href="http://www.aldeia.biz/" target="_blank" title="Explore &amp; Discover" rel="nofollow">Aldeia</a></p>
      </div>
    </div>
  </div>
</footer>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="<?php bloginfo( 'template_url' ); ?>/js/vendor/jquery.min.js">\x3C/script>')</script>
<script src="<?php bloginfo( 'template_url' ); ?>/js/plugins.js"></script>
<script src="<?php bloginfo( 'template_url' ); ?>/js/main.js"></script>
<script type="text/javascript" async="true" src="https://d335luupugsy2.cloudfront.net/js/loader-scripts/d49f8fd8-cb38-48b4-88d6-65256b5309c5-loader.js" ></script>
<?php wp_footer(); ?>

</body>
</html>