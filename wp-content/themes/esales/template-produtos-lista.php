<?php
/*
 * Template Name: Produtos Lista
 */
?>
<?php get_header(); ?>

<?php
while( have_posts() ) {
  the_post();

  $img_cartola = wp_get_attachment_image_src( get_field('imagem_cartola'), 'full' );

  $titulo_caracteristicas = get_field('titulo_caracteristicas');
  $texto_caracteristicas = get_field('texto_caracteristicas');

  $titulo_bloco_lateral = get_field('titulo_bloco_lateral');
  $texto_bloco_lateral = get_field('texto_bloco_lateral');

  $cases = get_field('cases');
  //$solucoes = get_field('solucoes');

  $share_links = get_share_links();
}
?>
<main>
  <div class="hero">
    <div class="overlay">
      <div class="container valign">
        <div class="center">
          <h2>Produtos</h2>
        </div><i></i>
      </div>
    </div>

    <div class="bg" style="background-image: url(<?php echo $img_cartola[0]; ?>);"></div>
  </div>

  <div class="main-content">
    <div class="bg-esq"></div>
    <div class="bg-dir"></div>
    <div class="bg-center">
      <div class="bg-center-inner">
        <svg class="img" viewBox="0 0 1920 657" style="background-color:#ffffff00" version="1.1"
          xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xml:space="preserve"
          x="0px" y="0px" width="1920px" height="657px">
          <path class="shape-bg" d="M 0 0 L 523 57 L 960 2.7641 L 1397 57 L 1920 3.191 L 1920 657 L 0 657 L 0 0 Z" fill="#ffffff"/>
        </svg>
      </div>
    </div>

    <div class="sec-produtos cf">
      <div class="col-left-full">
        <div class="col-left-full-inner">
          <h3><?php echo $titulo_bloco_lateral; ?></h3>
          <p><?php echo $texto_bloco_lateral; ?></p>
        </div>
        <div class="col-left-full-inner">
            <h3><?php echo $titulo_caracteristicas; ?></h3>
            <p><?php echo $texto_caracteristicas; ?></p>
        </div>
      </div>

      <div class="col-right-full">
        <div class="box-problema cf">
          <div class="content">
            <h2>Unidades de negócios</h2>
          </div>
        </div>

        <div class="box-lista-solucoes">
          <ul>
            <li>
              <a href="<?php echo get_permalink(285); ?>">
                <span><?php echo get_the_title(285); ?></span>
                <div class="icon-wrapper">
                  <img src="<?php bloginfo( 'template_url' ); ?>/img/icon-fiscal.png" alt="">
                </div>
              </a>
            </li>
            <li>
              <a href="<?php echo get_permalink(278); ?>">
                <span><?php echo get_the_title(278); ?></span>
                <div class="icon-wrapper">
                  <img src="<?php bloginfo( 'template_url' ); ?>/img/icon-grafico.png" alt="">
                </div>
              </a>
            </li>
            <li>
              <a href="<?php echo get_permalink(273); ?>">
                <span><?php echo get_the_title(273); ?></span>
                <div class="icon-wrapper">
                  <img src="<?php bloginfo( 'template_url' ); ?>/img/icon-transporte.png" alt="">
                </div>
              </a>
            </li>
            <li>
              <a href="<?php echo get_permalink(229); ?>">
                <span><?php echo get_the_title(229); ?></span>
                <div class="icon-wrapper">
                  <img src="<?php bloginfo( 'template_url' ); ?>/img/icon-arquivos.png" alt="">
                </div>
              </a>
            </li>
          </ul>
        </div>
      </div>
    </div>

    <div class="clear"></div>

    <?php get_template_part('inc-sec-contato'); ?>

    <div class="sec-outro">
      <div class="container">
        <div class="row">
          <div class="col-lg-12">
            <h2>Cases de sucesso</h2>
          </div>
        </div>
        <div class="row">
            <?php
            if( is_array( $cases ) && count( $cases ) ) {
              foreach ($cases as $case) {
                $texto_cartola = get_field('texto_cartola', $case->ID);
                $imagem_cartola = wp_get_attachment_image_src( get_field('imagem_cartola', $case->ID), 'solucoes_case');

                $clientes = wp_get_post_terms( $case->ID, 'clientes' );
                $cliente_logo = '';
                if( is_array( $clientes ) && count( $clientes ) > 0 ) {
                  $cliente_logo = wp_get_attachment_image_src( get_field('logo_branco', 'clientes_' . $clientes[0]->term_id), 'full' );
                  $cliente_logo = $cliente_logo[0];
                }
                ?>
                <div class="col-lg-6">
                  <a href="<?php echo get_permalink( $case->ID ); ?>" class="pic-wrapper">
                    <span class="pic">
                      <span class="overlay">
                        <h3>Qual era a necessidade?</h3>
                        <p><?php echo $texto_cartola; ?></p>
                      </span>

                      <img src="<?php echo $imagem_cartola[0]; ?>" alt="">
                    </span>

                    <?php
                    if($cliente_logo) {
                      ?>
                      <span class="caption caption-logo">
                        <span class="caption-inner">
                          <span class="logo-cliente valign">
                            <img src="<?php echo $cliente_logo; ?>" alt="" class="center"><i></i>
                          </span>
                          <i class="icon icon-arrow-right"></i>
                        </span>
                      </span>
                      <?php
                    }
                    ?>
                  </a>
                </div>
                <?php
              }
            }
            ?>
        <?php /*
          <div class="col-lg-5 col-right">
            <h2>Conheça também</h2>

            <?php
            if( is_array( $solucoes ) && count( $solucoes ) > 0 ) {
              foreach ($solucoes as $item) {
                $chamada_conheca_item = get_field('chamada_conheca', $item->ID);
                $imagem = wp_get_attachment_image_src( get_field( 'imagem' , $item->ID ), 'solucoes_solucoes' );

                ?>
                <a href="<?php echo get_permalink( $item->ID ); ?>" class="pic-wrapper">
                  <span class="pic">
                    <img src="<?php echo $imagem[0]; ?>" alt="">
                  </span>

                  <span class="caption">
                    <span class="caption-inner">
                      <h3><?php echo get_the_title( $item->ID ); ?></h3>
                      <p><?php echo $chamada_conheca_item; ?></p>
                      <i class="icon icon-arrow-right"></i>
                    </span>
                  </span>
                </a>
                <?php
              }
            }
            ?>
          </div>
          */?>
        </div>
      </div>
    </div>
  </div>
</main>

<?php get_footer(); ?>