/* globals jQuery */
(function($) {
  'use strict';

  $(function() {

    $('.vitrine').slick({
      arrows: false,
      dots: true,
      appendDots: '.vitrine-wrapper .dots',
      slide: '.vitrine-item',
      slidesToShow: 1,
      slidesToScroll: 1,
      speed: 1000,
      cssEase: 'cubic-bezier(0.645, 0.045, 0.355, 1)'
    });

    $('.depoimentos').slick({
      arrows: false,
      dots: true,
      appendDots: '.depoimentos-wrapper .dots',
      slide: '.depoimentos-item',
      slidesToShow: 1,
      slidesToScroll: 1,
      speed: 1000,
      cssEase: 'cubic-bezier(0.645, 0.045, 0.355, 1)'
    });

    var $tabWrapper     = $('#tab-wrapper'),
        $tabContent     = $('.tab-content'),
        $solucaoContent = $('#solucao-content'),
        $path           = $('#connector-path'),
        path            = $path[0],
        $navTabs        = $('.nav-tabs'),
        $tabPane        = $('.tab-pane'),
        points          = [12, 247, 480, 714, 948];

    $solucaoContent.hide();

    $navTabs.on('click', '.solucao-item', function(e) {
      e.preventDefault();

      var $this = $(this),
          tabName = $this.data('tab-name');

      if ( !$this.hasClass('ativo') ) {
        $tabContent.addClass('show');
        $('.solucao-default').addClass('hide');

        $this.siblings('.solucao-item.ativo').removeClass('ativo');
        $this.addClass('ativo');
        $tabWrapper.removeClass().addClass('tab-ativa-' + tabName);
        $tabPane.find('.item.ativo').removeClass('ativo');
        $path.attr('d', '');
        $solucaoContent.removeClass('done').delay(100).stop(true).slideUp(1000, 'easeInOutExpo');

        $('body,html').animate({
          scrollTop: $navTabs.offset().top - 10
        });
      } else {
        $tabContent.removeClass('show');
        $('.solucao-default').removeClass('hide');

        $this.removeClass('ativo');
        $tabWrapper.removeClass();
        $path.attr('d', '');
        $solucaoContent.removeClass('done').delay(100).stop(true).slideUp(1000, 'easeInOutExpo');
      }
    });

    $tabPane.on('click', '.item a', function(e) {
      e.preventDefault();

      var $item = $(this).closest('.item'),
          $solucaoAtivo = $navTabs.find('.solucao-item.ativo'),
          target = $(this).data('item-name') + '-' + $solucaoAtivo.data('tab-name');

      if ( !$item.hasClass('ativo') ) {
        $item.siblings('.item.ativo').removeClass('ativo');

        $solucaoContent.removeClass('done').delay(100).stop(true).slideUp(1000, 'easeInOutExpo', function() {
          //// target
          var targetURL = '_ajax_content.php';
          if( $('#segmentos-home').length ) {
            targetURL = $('#segmentos-home').data('url');
          }

          $.get( targetURL, { segmento : target } ).done(function(data) {
            $solucaoContent.html(data);
            $solucaoContent.stop(true).slideDown(1000, 'easeInOutExpo', function() {
              $(this).addClass('done');
            });
          });
        });

        var a = $solucaoAtivo.index('.solucao-item'),
            b = $item.index('.item');

        $path.attr('d', 'M '+points[a]+' 0 V 103.5 H '+points[b]+' V 194');

        var pathLength = path.getTotalLength();

        path.style.strokeDasharray = [pathLength, pathLength].join(' ');

        $({ pathLen: pathLength }).animate({
          pathLen: 0
        }, {
          easing: 'easeInOutQuad',
          duration: 350,
          step: function(now, fx) {
            path.style.strokeDashoffset = (now - fx.end) + 'px';
          },
          complete: function() {
            $item.addClass('ativo');
          }
        });
      }
    });


    /*======================================
      Clientes
    ======================================*/
    var $clienteCaption = $('#cliente-caption');

    $('.lista-clientes').on('mouseenter', '.logo-cliente[data-link]', function(e) {
      e.preventDefault();

      var $this = $(this),
          posLeft = $this.position().left;

      $this.siblings('.logo-cliente').removeClass('hover');
      $this.addClass('hover');

      $clienteCaption.find('h2').text( $this.data('title') );
      $clienteCaption.find('p').text( $this.data('content') );
      $clienteCaption.find('a').prop('href', $this.data('link') );

      if ( posLeft <= 400 ) {
        $clienteCaption.css({
          left: posLeft
        });
      } else {
        $clienteCaption.css({
          left: (posLeft - $clienteCaption.outerWidth()) + $this.outerWidth()
        });
      }

      $clienteCaption.addClass('show');
    });

    $('.lista-clientes-wrapper').on('mouseleave', function() {
      $clienteCaption.removeClass('show');
      $('.lista-clientes').find('.logo-cliente.hover').removeClass('hover');
    });

    $('.lista-clientes').slick({
      speed: 200,
      slide: '.logo-cliente',
      slidesToShow: 3,
      slidesToScroll: 1,
      prevArrow: '<button type="button" class="slick-prev"><div class="icon icon-triangle-left"></div></button>',
      nextArrow: '<button type="button" class="slick-next"><div class="icon icon-triangle-right"></div></button>'
    }).on('beforeChange', function() {
      $clienteCaption.removeClass('show');
      $(this).find('.logo-cliente.hover').removeClass('hover');
    });


    /*======================================
      Cases Detalhe
    ======================================*/
    var $slideWrapper = $('.slide-wrapper'),
        $pag = $('.slide-controls').find('.pag');

    $slideWrapper.on('init', function(e, slick) {
      $pag.find('.curr').text(slick.currentSlide+1);
      $pag.find('.total').text(slick.slideCount);
    });

    $slideWrapper.on('beforeChange', function(e, slick, currentSlide, nextSlide) {
      $pag.find('.curr').text(nextSlide+1);
    });

    $slideWrapper.slick({
      slide: '.slide-item',
      slidesToShow: 1,
      slidesToScroll: 1,
      speed: 1000,
      cssEase: 'cubic-bezier(0.645, 0.045, 0.355, 1)',
      prevArrow: '.slide-controls .bt-prev',
      nextArrow: '.slide-controls .bt-next'
    });

  });
}(jQuery));
